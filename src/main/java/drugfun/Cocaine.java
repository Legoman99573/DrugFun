package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class Cocaine implements Listener {
    private static Main main;
    public ArrayList<Player> CCooldown = new ArrayList();
    public ArrayList<Player> CSyrCooldown = new ArrayList();
    Items items = new Items();
    Random r = new Random();

    public Cocaine(Main main) {
        Cocaine.main = main;
    }

    @EventHandler
    public void SyringeUseCocaine(final PlayerInteractEvent e) {
        if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && Objects.equals(e.getHand(), EquipmentSlot.HAND) && e.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getItemInHand().getItemMeta(), this.items.getSyrCocaineMeta())) {
            if (!e.getPlayer().hasPermission("drugfun.consumecocaine")) {
                e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume")), main));
                e.setCancelled(true);
                return;
            }

            if (this.CSyrCooldown.contains(e.getPlayer())) {
                return;
            }

            e.setCancelled(true);
            e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
            e.getPlayer().addPotionEffects(EffectsManager.CocaineEffects);
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
            e.getPlayer().damage(5.0D);
            e.getPlayer().sendMessage(Texts.cocaineSyringeInject);
            SlurManager.addSlurFinish(e.getPlayer(), System.currentTimeMillis() + (long)(Main.cocaineSlurTime * 1000.0D));
            SlurManager.setEffect(e.getPlayer(), Main.cocaineSlur);

            if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setCocaine(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getCocaine() + 10);
            }

            Drugs.isOverdose(e.getPlayer(), Drugs.COCAINE);
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    if (Cocaine.main.acs) {
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                    }

                }
            }, 8L);
            this.CSyrCooldown.add(e.getPlayer());
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    Cocaine.this.CSyrCooldown.remove(e.getPlayer());
                }
            }, 20L);
        }

    }

    @EventHandler
    public void InjectIntoEntites(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
            Player p = (Player)e.getDamager();
            LivingEntity et = (LivingEntity)e.getEntity();
            if (p.getInventory().getItemInHand().hasItemMeta() && Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.items.getSyrCocaineMeta())) {
                if (!p.hasPermission("drugfun.consumecocaine")) {
                    p.sendMessage(HexChat.translateHexCodes( main.getConfig().getString("nopermstoconsume"), main));
                    e.setCancelled(true);
                    return;
                }

                p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
                e.setCancelled(true);
                et.addPotionEffects(EffectsManager.CocaineEffects);
                p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
                p.sendMessage(Texts.cocaineSyringeInjectAttacker);
                et.sendMessage(Texts.cocaineSyringeInjectVictim);
                if (Main.bloods.containsKey(et.getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(et.getUniqueId())).setCocaine(((DrugBloodManager)Main.bloods.get(et.getUniqueId())).getCocaine() + 10);
                }

                SlurManager.addSlurFinish((Player)et, System.currentTimeMillis() + (long)(Main.cocaineSlurTime * 1000.0D));
                SlurManager.setEffect((Player)et, Main.cocaineSlur);
                Drugs.isOverdose((Player)et, Drugs.COCAINE);
            }
        }

    }

    @EventHandler
    public void CocaineSnort(final PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            final Player pl = e.getPlayer();
            if (pl.getItemInHand().hasItemMeta() && Objects.equals(pl.getItemInHand().getItemMeta(), this.items.getCitemmeta()) && Objects.equals(e.getHand(), EquipmentSlot.HAND) && !this.CCooldown.contains(e.getPlayer())) {
                if (!e.getPlayer().hasPermission("drugfun.consumecocaine")) {
                    e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Cocaine")), main));
                    e.setCancelled(true);
                    return;
                }

                e.setCancelled(true);
                pl.getInventory().getItemInHand().setAmount(pl.getInventory().getItemInHand().getAmount() - 1);
                pl.addPotionEffects(EffectsManager.CocaineEffects);
                if (main.cs) {
                    pl.playSound(pl.getLocation(), Sound.ENTITY_GENERIC_DRINK, 1.0F, 0.7F);
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        if (Cocaine.main.acs) {
                            pl.playSound(pl.getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                        }

                    }
                }, 8L);
                pl.sendMessage(Texts.cocaineSnort);
                SlurManager.addSlurFinish(e.getPlayer(), System.currentTimeMillis() + (long)(Main.cocaineSlurTime * 1000.0D));
                SlurManager.setEffect(e.getPlayer(), Main.cocaineSlur);
                this.CCooldown.add(e.getPlayer());
                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        Cocaine.this.CCooldown.remove(e.getPlayer());
                    }
                }, 20L);
            }
        }

    }

    @EventHandler
    public void onCocaPlant(BlockPlaceEvent event) {
        Items i = new Items();
        Player p = event.getPlayer();
        Block block = event.getBlock();
        if (p.getItemInHand() != null && p.getItemInHand().hasItemMeta() && Objects.equals(p.getItemInHand().getItemMeta(), i.getCpitemmeta())) {
            if (block.getBlockData() instanceof Bisected && ((Bisected)block.getBlockData()).getHalf() == Half.TOP) {
                block = block.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock();
            }
            event.getPlayer().sendMessage(Texts.cocaPlant);
            FileManager.putLocation(block.getLocation(), (long)((double)System.currentTimeMillis() + main.CocaTime * 60.0D * 1000.0D));
            FileManager.putLocation(block.getLocation().add(0.0D, 1.0D, 0.0D), (long)((double)System.currentTimeMillis() + main.CocaTime * 60.0D * 1000.0D));
            if (main.dps) {
                p.playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }

    }

    @EventHandler
    public void CocaineHarvest(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (Objects.equals(e.getHand(), EquipmentSlot.HAND)) {
                if (Objects.requireNonNull(e.getClickedBlock()).getType().equals(Material.TALL_GRASS) && FileManager.isValidLocation(e.getClickedBlock().getLocation())) {
                    if (System.currentTimeMillis() >= FileManager.getLocationTime(e.getClickedBlock().getLocation())) {
                        Objects.requireNonNull(e.getClickedBlock().getLocation().getWorld()).dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getClitem(main.CocaLeafAmount));
                        if (((ItemStack)Main.cocaineList.get(this.r.nextInt(Main.cocaineList.size() - 1))).getType() == Material.DIAMOND) {
                            e.getClickedBlock().getLocation().getWorld().dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getCpitem(main.cocaplantAmount));
                        }

                        FileManager.setTime(e.getClickedBlock().getLocation(), (long)((double)System.currentTimeMillis() + main.CocaTime * 60.0D * 1000.0D));
                        e.getPlayer().sendMessage(Texts.cocaHarvest);
                        if (main.dhs) {
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.7F);
                        }
                    } else {
                        e.getPlayer().sendMessage(Texts.cocaHarvestFail);
                    }
                }

            }
        }
    }

    @EventHandler
    public void CocaineBreak(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.TALL_GRASS && FileManager.isValidLocation(e.getBlock().getLocation())) {
            FileManager.removeLocation(e.getBlock().getLocation());
            e.setDropItems(false);
        }

    }

    @EventHandler
    public void CocaineWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.TALL_GRASS && FileManager.isValidLocation(block.getLocation())) {
            if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                e.setCancelled(true);
                block.setType(Material.AIR);
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
            }

            if (FileManager.isValidLocation(block.getLocation())) {
                FileManager.removeLocation(block.getLocation());
                e.setCancelled(true);
                block.setType(Material.AIR);
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
            }
        }
    }

    @EventHandler
    public void CocaineExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.TALL_GRASS) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
                }
            }
        }

    }

    @EventHandler
    public void CocaineRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block) var2.next();
            if (block.getType() == Material.TALL_GRASS) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getCpitem(1));
                }
            }
        }
    }

    @EventHandler
    public void CocaineEntityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.TALL_GRASS && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float)0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.items.getCpitem(1));
            }
        }

    }
}