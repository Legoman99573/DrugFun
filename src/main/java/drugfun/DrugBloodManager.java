package drugfun;

public class DrugBloodManager {
    private int cocaine = 0;
    private int ecstasy = 0;
    private int lsd = 0;
    private int weed = 0;
    private int heroin = 0;
    private int shrooms = 0;
    private int meth = 0;

    DrugBloodManager() {
    }

    public int getHeroin() {
        return this.heroin;
    }

    public void setHeroin(int heroin) {
        this.heroin = heroin;
    }

    public int getEcstasy() {
        return this.ecstasy;
    }

    public void setEcstasy(int ecstasy) {
        this.ecstasy = ecstasy;
    }

    public int getLsd() {
        return this.lsd;
    }

    public void setLsd(int lsd) {
        this.lsd = lsd;
    }

    public int getCocaine() {
        return this.cocaine;
    }

    public void setCocaine(int cocaine) {
        this.cocaine = cocaine;
    }

    public int getShrooms() {
        return this.shrooms;
    }

    public void setShrooms(int shrooms) {
        this.shrooms = shrooms;
    }

    public int getMeth() {
        return this.meth;
    }

    public void setMeth(int meth) {
        this.meth = meth;
    }

    public int getWeed() {
        return this.weed;
    }

    public void setWeed(int weed) {
        this.weed = weed;
    }
}
