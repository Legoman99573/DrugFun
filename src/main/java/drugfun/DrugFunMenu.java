package drugfun;

import java.util.Iterator;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DrugFunMenu implements CommandExecutor {
    private Main main;
    Items i = new Items();

    public DrugFunMenu(Main main) {
        this.main = main;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player)sender;
            LSD lsd = new LSD(this.main);
            if (args.length == 0) {
                sender.sendMessage(Texts.prefix + ChatColor.RED + "Version: 2.4 | By: [@realFirstix] on www.spigotmc.org");
            } else {
                if (args[0].equalsIgnoreCase("bloodtest")) {
                    if (p.hasPermission("drugfun.bloodtest")) {
                        if (args.length > 1) {
                            Player bloodplayer = Bukkit.getPlayerExact(args[1]);
                            if (bloodplayer == null) {
                                p.sendMessage(Texts.prefix + HexChat.translateHexCodes((String) Objects.requireNonNull(this.main.getConfig().getString("playerNotOnline").replace("%target-player%", args[1])), main));
                                return false;
                            }
                            if (Main.bloods.containsKey(bloodplayer.getUniqueId())) {
                                Main.getInfo(p, (Player) bloodplayer);
                                return true;
                            }
                        } else {
                            p.sendMessage(Texts.helpbloodtest);
                        }

                    } else {
                        p.sendMessage(Texts.nopermission);
                    }
                    return true;
                }
                if (args[0].equalsIgnoreCase("drugdealer")) {
                    LivingEntity dd;
                    if (args.length > 1) {
                        if (args[1].equalsIgnoreCase("spawn-villager")) {
                            if (p.hasPermission("drugfun.drugdealerspawn")) {
                                dd = (LivingEntity) ((World) Objects.requireNonNull(p.getLocation().getWorld())).spawnEntity(p.getLocation(), EntityType.VILLAGER);
                                dd.setCustomNameVisible(true);
                                dd.setCustomName(Texts.drugdealername);
                                dd.setInvulnerable(true);
                                dd.setGravity(false);
                                dd.setSilent(true);
                                dd.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999999, 999999999, false, false));
                                dd.setAI(false);
                                dd.setRemoveWhenFarAway(false);
                                p.sendMessage(Texts.drugDealerSpawn);
                            } else {
                                p.sendMessage(Texts.nopermission);
                            }
                            return false;
                        }
                        if (args[1].equalsIgnoreCase("spawn-wanderer")) {
                            if (p.hasPermission("drugfun.drugdealerspawn")) {
                                dd = (LivingEntity) ((World) Objects.requireNonNull(p.getLocation().getWorld())).spawnEntity(p.getLocation(), EntityType.WANDERING_TRADER);
                                dd.setCustomNameVisible(true);
                                dd.setCustomName(Texts.drugdealername);
                                dd.setInvulnerable(true);
                                dd.setGravity(false);
                                dd.setSilent(true);
                                dd.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999999, 999999999, false, false));
                                dd.setAI(false);
                                dd.setRemoveWhenFarAway(false);
                                p.sendMessage(Texts.drugDealerSpawn);
                            } else {
                                p.sendMessage(Texts.nopermission);
                            }
                            return true;
                        }
                        if (args[1].equalsIgnoreCase("spawn-piglin")) {
                            if (p.hasPermission("drugfun.drugdealerspawn")) {
                                dd = (LivingEntity) ((World) Objects.requireNonNull(p.getLocation().getWorld())).spawnEntity(p.getLocation(), EntityType.PIGLIN);
                                dd.setCustomNameVisible(true);
                                dd.setCustomName(Texts.drugdealername);
                                dd.setInvulnerable(true);
                                dd.setGravity(false);
                                dd.setSilent(true);
                                dd.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999999, 999999999, false, false));
                                dd.setAI(false);
                                dd.setRemoveWhenFarAway(false);
                                p.sendMessage(Texts.drugDealerSpawn);
                            } else {
                                p.sendMessage(Texts.nopermission);
                            }
                            return true;
                        }
                        if (args[1].equalsIgnoreCase("remove")) {
                            if (p.hasPermission("drugfun.drugdealerremove")) {
                                Iterator var7 = ((World) Objects.requireNonNull(p.getLocation().getWorld())).getNearbyEntities(p.getLocation(), 2.5D, 2.5D, 2.5D).iterator();

                                while (var7.hasNext()) {
                                    Entity e = (Entity) var7.next();
                                    if (e.getName().contains(Texts.drugdealername)) {
                                        e.remove();
                                        p.sendMessage(Texts.drugdealerremoved);
                                    }
                                }

                            } else {
                                p.sendMessage(Texts.nopermission);
                            }
                            return false;
                        }
                    } else {
                        p.sendMessage(Texts.helpdrugdealer);
                        return false;
                    }
                } else if (args[0].equals("give")) {
                    if (sender.hasPermission("drugfun.give")) {
                        if (args.length > 3) {
                            Player giveplayer = Bukkit.getPlayerExact(args[1]);
                            if (giveplayer == null) {
                                p.sendMessage(Texts.prefix + HexChat.translateHexCodes((String) Objects.requireNonNull(this.main.getConfig().getString("playerNotOnline").replace("%target-player%", args[1])), main));
                            } else {
                                Player gp = Bukkit.getPlayerExact(args[1]);
                                Integer amt = Integer.parseInt(args[3]);
                                String var8;
                                switch ((var8 = args[2].toUpperCase()).hashCode()) {
                                    case -2129982686:
                                        if (var8.equals("IODINE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getIitem(amt)});
                                        }
                                        break;
                                    case -2127110210:
                                        if (var8.equals("MORNINGGLORYPLANT")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getMorningGloryPlant(amt)});
                                        }
                                        break;
                                    case -2119920646:
                                        if (var8.equals("OPIUMPOPPYSEEDPOD")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getOpspitem(amt)});
                                        }
                                        break;
                                    case -1849732944:
                                        if (var8.equals("SHROOM")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getShroomItem(amt)});
                                        }
                                        break;
                                    case -1718543314:
                                        if (var8.equals("SAFROLE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSafrole(amt)});
                                        }
                                        break;
                                    case -1574482172:
                                        if (var8.equals("MORNINGGLORYPLANTSEEDS")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getMorningGloryPlantSeeds(amt)});
                                        }
                                        break;
                                    case -1461968661:
                                        if (var8.equals("LYSERGICACID")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getLysergicAcid(amt)});
                                        }
                                        break;
                                    case -1189383016:
                                        if (var8.equals("ECSTACY")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getEcitem(amt)});
                                        }
                                        break;
                                    case -1020630673:
                                        if (var8.equals("SYRINGE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSyrItem(amt)});
                                        }
                                        break;
                                    case -952918390:
                                        if (var8.equals("EPHEDRINE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getEitem(amt)});
                                        }
                                        break;
                                    case -873195883:
                                        if (var8.equals("PHOSPHORUS")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getPitem(amt)});
                                        }
                                        break;
                                    case -827806325:
                                        if (var8.equals("EPHEDRA")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getEsitem(amt)});
                                        }
                                        break;
                                    case -713399357:
                                        if (var8.equals("ETHANOL")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getEthanol(amt)});
                                        }
                                        break;
                                    case -334616581:
                                        if (var8.equals("SYRINGEMETH")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSyrMethItem(amt)});
                                        }
                                        break;
                                    case -116963288:
                                        if (var8.equals("COCALEAF")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getClitem(amt)});
                                        }
                                        break;
                                    case 71345:
                                        if (var8.equals("HCL")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getHclitem(amt)});
                                        }
                                        break;
                                    case 75677:
                                        if (var8.equals("LSD")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getAcid(amt)});
                                        }
                                        break;
                                    case 2362892:
                                        if (var8.equals("METH")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getMethitem(amt)});
                                        }
                                        break;
                                    case 2660333:
                                        if (var8.equals("WEED")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getWitem(amt)});
                                        }
                                        break;
                                    case 41882983:
                                        if (var8.equals("SYRINGECOCAINE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSyrCocaineItem(amt)});
                                        }
                                        break;
                                    case 63300689:
                                        if (var8.equals("BLUNT")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getBluntitem(amt)});
                                        }
                                        break;
                                    case 75414304:
                                        if (var8.equals("OPIUM")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getOitem(amt)});
                                        }
                                        break;
                                    case 412816590:
                                        if (var8.equals("SYRINGEHEROIN")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSyrHeroinItem(amt)});
                                        }
                                        break;
                                    case 496318078:
                                        if (var8.equals("WEEDPLANT")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getWsitem(amt)});
                                        }
                                        break;
                                    case 574446040:
                                        if (var8.equals("MORPHINE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getMitem(amt)});
                                        }
                                        break;
                                    case 673008321:
                                        if (var8.equals("COCAPLANT")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getCpitem(amt)});
                                        }
                                        break;
                                    case 886622089:
                                        if (var8.equals("SASSAFRASBARK")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSbark(amt)});
                                        }
                                        break;
                                    case 1510699079:
                                        if (var8.equals("SASSAFRASSAPLING")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getSsitem(amt)});
                                        }
                                        break;
                                    case 1544584693:
                                        if (var8.equals("METHYLAMINECHLORIDE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getMethylChloride(amt)});
                                        }
                                        break;
                                    case 1556420393:
                                        if (var8.equals("CHLOROFORM")) {
                                            gp.getInventory().addItem(new ItemStack[]{lsd.getChloroform(amt)});
                                        }
                                        break;
                                    case 1658792374:
                                        if (var8.equals("COCAINE")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getCitem(amt)});
                                        }
                                        break;
                                    case 1671527155:
                                        if (var8.equals("MERCURY")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getMercury(amt)});
                                        }
                                        break;
                                    case 1873129690:
                                        if (var8.equals("OPIUMPOPPY")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getOpitem(amt)});
                                        }
                                        break;
                                    case 2127542943:
                                        if (var8.equals("HEROIN")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getHitem(amt)});
                                        }
                                        break;
                                    case 2141007930:
                                        if (var8.equals("LITBLUNT")) {
                                            gp.getInventory().addItem(new ItemStack[]{this.i.getLitBlunt(amt)});
                                        }
                                }
                            }
                        } else {
                            // TODO Fix this broken shit as its always throwing org.bukkit.command.CommandException
                            p.sendMessage(Texts.helpgive);
                        }
                    } else {
                        p.sendMessage(Texts.nopermission);
                    }
                    return false;
                } else if (args[0].equals("menu")) {
                    if (p.hasPermission("drugfun.menu")) {
                        this.main.DrugMenu((Player) sender);
                    } else {
                        sender.sendMessage(Texts.nopermission);
                    }
                    return false;
                } else if (args[0].equals("shop")) {
                    DrugShop ds = new DrugShop(this.main);
                    if (p.hasPermission("drugfun.shop")) {
                        ds.openMainMenu(p);
                    } else {
                        p.sendMessage(Texts.nopermission);
                    }
                    return false;
                } else if (args[0].equals("forcesave")) {
                    if (p.hasPermission("drugfun.forcesave")) {
                        FileManager.saveOnForceSave();
                        p.sendMessage(Texts.forcesave);
                    } else {
                        p.sendMessage(Texts.nopermission);
                    }
                    return false;
                } else {
                    if (args[0].equals("help")) {
                        p.sendMessage(Texts.prefix);
                        p.sendMessage(Texts.help);
                        if (p.hasPermission("drugfun.menu")) {
                            p.sendMessage(Texts.helpmenu);
                        }

                        if (p.hasPermission("drugfun.shop")) {
                            p.sendMessage(Texts.helpshop);
                        }

                        if (p.hasPermission("drugfun.give")) {
                            p.sendMessage(Texts.helpgive);
                        }

                        if (p.hasPermission("drugfun.drugdealerspawn") && p.hasPermission("drugfun.drugdealerremove")) {
                            p.sendMessage(Texts.helpdrugdealer);
                        }

                        if (p.hasPermission("drugfun.forcesave")) {
                            p.sendMessage(Texts.helpforcesave);
                        }

                        if (p.hasPermission("drugfun.bloodtest")) {
                            p.sendMessage(Texts.helpbloodtest);
                        }
                    }
                }
            }
        } else {
            sender.sendMessage(Texts.prefix + ChatColor.RED + "Version: 2.4 | By: [@realFirstix] on www.spigotmc.org");
        }
        return true;
    }
}