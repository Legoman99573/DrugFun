package drugfun;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class DrugFunTabComplete implements TabCompleter {

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase("drugfun")) {
            if (args.length == 1) {
                List<String> arguments1 = new ArrayList<String>();
                if (sender.hasPermission("drugfun.bloodtest")) {
                    arguments1.add("bloodtest");
                }
                if (sender.hasPermission("drugfun.drugdealerspawn") || sender.hasPermission("drugfun.drugdealerremove")) {
                    arguments1.add("drugdealer");
                }
                if (sender.hasPermission("drugfun.forcesave")) {
                    arguments1.add("forcesave");
                }
                if (sender.hasPermission("drugfun.give")) {
                    arguments1.add("give");
                }
                arguments1.add("help");
                if (sender.hasPermission("drugfun.menu")) {
                    arguments1.add("menu");
                }
                if (sender.hasPermission("drugfun.shop")) {
                    arguments1.add("shop");
                }
                return arguments1;
            }
            if (args.length == 2) {
                if (args[0].equals("drugdealer")) {
                    List<String> drugdealerspawnremove = new ArrayList<String>();
                    if (sender.hasPermission("drugfun.drugdealerspawn")) {
                        drugdealerspawnremove.add("spawn-villager");
                        drugdealerspawnremove.add("spawn-wanderer");
                        drugdealerspawnremove.add("spawn-piglin");
                    }
                    if (sender.hasPermission("drugfun.drugdealerremove")) {
                        drugdealerspawnremove.add("remove");
                    }

                    return drugdealerspawnremove;
                }

                if (args[0].equals("give")) {
                    List<String> playerNames = new ArrayList<String>();
                    Player[] players = new Player[Bukkit.getServer().getOnlinePlayers().size()];
                    Bukkit.getServer().getOnlinePlayers().toArray(players);
                    if (sender.hasPermission("drugfun.give")) {
                        for (int i = 0; i < players.length; i++) {
                            playerNames.add(players[i].getName());
                        }
                    }
                    return playerNames;
                }
                if (args[0].equals("bloodtest")) {
                    List<String> playerNames = new ArrayList<String>();
                    Player[] players = new Player[Bukkit.getServer().getOnlinePlayers().size()];
                    Bukkit.getServer().getOnlinePlayers().toArray(players);
                    if (sender.hasPermission("drugfun.bloodtest")) {
                        for (int i = 0; i < players.length; i++) {
                            playerNames.add(players[i].getName());
                        }
                    }
                    return playerNames;
                }

            }

            if (args.length == 3) {
                if (args[0].equals("give")) {
                    List<String> drugnames = new ArrayList<String>();
                    if (sender.hasPermission("drugfun.give")) {
                        drugnames.add("Blunt");
                        drugnames.add("Chloroform");
                        drugnames.add("Cocaine");
                        drugnames.add("CocaLeaf");
                        drugnames.add("CocaPlant");
                        drugnames.add("Ecstacy");
                        drugnames.add("Ephedra");
                        drugnames.add("Ephedrine");
                        drugnames.add("Ethanol");
                        drugnames.add("Heroin");
                        drugnames.add("HydrochloricAcid");
                        drugnames.add("Iodine");
                        drugnames.add("LSD");
                        drugnames.add("LysergicAcid");
                        drugnames.add("Marijuana");
                        drugnames.add("MarijuanaPlant");
                        drugnames.add("Mercury");
                        drugnames.add("Methamphetamine");
                        drugnames.add("MethylamineChloride");
                        drugnames.add("MorningGloryPlant");
                        drugnames.add("MorningGloryPlantSeeds");
                        drugnames.add("Morphine");
                        drugnames.add("Opium");
                        drugnames.add("OpiumPoppy");
                        drugnames.add("OpiumPoppySeedPods");
                        drugnames.add("OpiumPoppySeedPods");
                        drugnames.add("Phosphorus");
                        drugnames.add("PsilocybinMushroom");
                        drugnames.add("Safrole");
                        drugnames.add("SassafrasBark");
                        drugnames.add("SassafrasSapling");
                        drugnames.add("Syringe");
                    }
                    return drugnames;
                }
            }

            if (args.length == 4) {
                if (args[0].equals("give")) {
                    List<String> drugamount = new ArrayList<String>();
                    if (sender.hasPermission("drugfun.give")) {
                        drugamount.add("Amount");
                    }
                    return drugamount;
                }
            }
        }
        return null;
    }
}
