package drugfun;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DrugShop implements Listener {
    private Main main;
    Items items = new Items();
    LSD lsd;

    DrugShop(Main main) {
        this.lsd = new LSD(this.main);
        this.main = main;
    }

    public void openMainMenu(Player p) {
        Inventory i = Bukkit.createInventory((InventoryHolder)null, 9, HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("DrugShopMenuName")), this.main));
        ItemStack sellButton = new ItemStack(Material.EMERALD_BLOCK);
        ItemStack buyButton = new ItemStack(Material.REDSTONE_BLOCK);
        ItemStack closeButton = new ItemStack(Material.BARRIER);
        ItemMeta sellButtonMeta = sellButton.getItemMeta();
        ItemMeta buyButtonMeta = buyButton.getItemMeta();
        ItemMeta closeButtonMeta = closeButton.getItemMeta();
        sellButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellButtonName")), this.main));
        buyButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("BuyButtonName")), this.main));
        closeButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("CloseButtonName")), this.main));
        sellButton.setItemMeta(sellButtonMeta);
        buyButton.setItemMeta(buyButtonMeta);
        closeButton.setItemMeta(closeButtonMeta);
        i.setItem(0, sellButton);
        i.setItem(1, buyButton);
        i.setItem(8, closeButton);
        p.openInventory(i);
    }

    public void openSellMenu(Player p) {
        Inventory sellmenu = Bukkit.createInventory((InventoryHolder)null, 54, HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellMenuTitle")), this.main));
        ItemStack sellButton = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta sellButtonMeta = sellButton.getItemMeta();
        sellButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellInstructions")), this.main));
        sellButton.setItemMeta(sellButtonMeta);
        sellmenu.setItem(52, sellButton);
        ItemStack returnButton = new ItemStack(Material.BARRIER);
        ItemMeta returnButtonMeta = returnButton.getItemMeta();
        returnButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("CloseButtonName")), this.main));
        returnButton.setItemMeta(returnButtonMeta);
        sellmenu.setItem(53, returnButton);
        p.openInventory(sellmenu);
    }

    public void openBuyMenu(Player p) {
        Inventory buymenu = Bukkit.createInventory((InventoryHolder)null, 27, HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("BuyMenuTitle")), this.main));
        ItemStack buyButton = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta buyButtonMeta = buyButton.getItemMeta();
        buyButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("BuyInstructions")), this.main));
        ItemStack returnButton = new ItemStack(Material.BARRIER);
        ItemMeta returnButtonMeta = returnButton.getItemMeta();
        returnButtonMeta.setDisplayName(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("CloseButtonName")), this.main));
        returnButton.setItemMeta(returnButtonMeta);
        buymenu.setItem(26, returnButton);
        buymenu.setItem(0, this.items.getCitem(1));
        buymenu.setItem(1, this.items.getHitem(1));
        buymenu.setItem(2, this.items.getWitem(1));
        buymenu.setItem(3, this.items.getMethitem(1));
        buymenu.setItem(4, this.items.getShroomItem(1));
        buymenu.setItem(9, this.items.getCpitem(1));
        buymenu.setItem(10, this.items.getOpitem(1));
        buymenu.setItem(12, this.items.getEsitem(1));
        buymenu.setItem(11, this.items.getWsitem(1));
        buymenu.setItem(5, this.items.getEcitem(1));
        buymenu.setItem(14, this.items.getSsitem(1));
        buymenu.setItem(6, this.lsd.getAcid(1));
        buymenu.setItem(15, this.lsd.getMorningGloryPlant(1));
        ItemStack cocaine = buymenu.getItem(0);
        ItemMeta cocaineMeta = cocaine.getItemMeta();
        ArrayList<String> cocaineLore = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Cocaine")) {
            cocaineLore.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            cocaineLore.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Cocaine"));
        }

        cocaineMeta.setLore(cocaineLore);
        cocaine.setItemMeta(cocaineMeta);
        ItemStack heroin = buymenu.getItem(1);
        ItemMeta heroinMeta = heroin.getItemMeta();
        ArrayList<String> heroinLore = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Heroin")) {
            heroinLore.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            heroinLore.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Heroin"));
        }

        heroinMeta.setLore(heroinLore);
        heroin.setItemMeta(heroinMeta);
        ItemStack weed = buymenu.getItem(2);
        ItemMeta weedMeta = weed.getItemMeta();
        ArrayList<String> weedLore = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Weed")) {
            weedLore.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            weedLore.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Weed"));
        }

        weedMeta.setLore(weedLore);
        weed.setItemMeta(weedMeta);
        ItemStack meth = buymenu.getItem(3);
        ItemMeta methMeta = meth.getItemMeta();
        ArrayList<String> methLore = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Meth")) {
            methLore.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            methLore.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Meth"));
        }

        methMeta.setLore(methLore);
        meth.setItemMeta(methMeta);
        ItemStack shroom = buymenu.getItem(4);
        ItemMeta shroomMeta = shroom.getItemMeta();
        ArrayList<String> shroomLore = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Shrooms")) {
            shroomLore.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            shroomLore.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Shrooms"));
        }

        shroomMeta.setLore(shroomLore);
        shroom.setItemMeta(shroomMeta);
        ItemStack cp = buymenu.getItem(9);
        ItemMeta cpM = cp.getItemMeta();
        ArrayList<String> cpL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.CocaPlant")) {
            cpL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            cpL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.CocaPlant"));
        }

        cpM.setLore(cpL);
        cp.setItemMeta(cpM);
        ItemStack op = buymenu.getItem(10);
        ItemMeta opM = op.getItemMeta();
        ArrayList<String> opL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.OpiumPoppy")) {
            opL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            opL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.OpiumPoppy"));
        }

        opM.setLore(opL);
        op.setItemMeta(opM);
        ItemStack es = buymenu.getItem(12);
        ItemMeta esM = es.getItemMeta();
        ArrayList<String> esL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Ephedra")) {
            esL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            esL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Ephedra"));
        }

        esM.setLore(esL);
        es.setItemMeta(esM);
        ItemStack ws = buymenu.getItem(11);
        ItemMeta wsM = ws.getItemMeta();
        ArrayList<String> wsL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.WeedPlant")) {
            wsL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            wsL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.WeedPlant"));
        }

        wsM.setLore(wsL);
        ws.setItemMeta(wsM);
        ItemStack e = buymenu.getItem(5);
        ItemMeta eM = e.getItemMeta();
        ArrayList<String> eL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.Ecstacy")) {
            eL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            eL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.Ecstacy"));
        }

        eM.setLore(eL);
        e.setItemMeta(eM);
        ItemStack ss = buymenu.getItem(14);
        ItemMeta ssM = ss.getItemMeta();
        ArrayList<String> ssL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.SassafrasSapling")) {
            ssL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            ssL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.SassafrasSapling"));
        }

        ssM.setLore(ssL);
        ss.setItemMeta(ssM);
        ItemStack a = buymenu.getItem(6);
        ItemMeta aM = a.getItemMeta();
        ArrayList<String> aL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.LSD")) {
            aL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            aL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.LSD"));
        }

        aM.setLore(aL);
        a.setItemMeta(aM);
        ItemStack mg = buymenu.getItem(15);
        ItemMeta mgM = mg.getItemMeta();
        ArrayList<String> mgL = new ArrayList();
        if (!this.main.getConfig().getBoolean("enableBuying.MorningGloryPlant")) {
            mgL.add(ChatColor.RED + "NOT FOR SALE");
        } else {
            mgL.add(ChatColor.RED + "$ " + ChatColor.RED + this.main.getConfig().getDouble("Buying.MorningGloryPlant"));
        }

        mgM.setLore(mgL);
        mg.setItemMeta(mgM);
        buymenu.setItem(0, cocaine);
        buymenu.setItem(1, heroin);
        buymenu.setItem(2, weed);
        buymenu.setItem(3, meth);
        buymenu.setItem(4, shroom);
        buymenu.setItem(9, cp);
        buymenu.setItem(10, op);
        buymenu.setItem(12, es);
        buymenu.setItem(11, ws);
        buymenu.setItem(5, e);
        buymenu.setItem(14, ss);
        buymenu.setItem(6, a);
        buymenu.setItem(15, mg);
        p.openInventory(buymenu);
    }

    @EventHandler
    public void MenuInteractions(InventoryClickEvent e) {
        if (e.getView().getTitle().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("DrugShopMenuName")), this.main)) && e.getCurrentItem() != null && e.getCurrentItem().getType() != null) {
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellButtonName")), this.main))) {
                e.setCancelled(true);
                if (((Player)e.getWhoClicked()).hasPermission("drugfun.shopsell")) {
                    this.openSellMenu((Player)e.getWhoClicked());
                } else {
                    ((Player)e.getWhoClicked()).sendMessage(Texts.nopermission);
                }
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("BuyButtonName")), this.main))) {
                e.setCancelled(true);
                if (((Player)e.getWhoClicked()).hasPermission("drugfun.shopbuy")) {
                    this.openBuyMenu((Player)e.getWhoClicked());
                } else {
                    ((Player)e.getWhoClicked()).sendMessage(Texts.nopermission);
                }
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("CloseButtonName")), this.main))) {
                e.setCancelled(true);
                e.getWhoClicked().closeInventory();
            }
        }

        if (e.getView().getTitle().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellMenuTitle")), this.main))) {
            Double total = 0.0D;
            if (e.getSlot() == 52) {
                total = 0.0D;
                e.setCancelled(true);
                ItemStack[] var7;
                int var6 = (var7 = e.getClickedInventory().getContents()).length;

                for(int var5 = 0; var5 < var6; ++var5) {
                    ItemStack i = var7[var5];
                    if (i != null && i.hasItemMeta()) {
                        if (i.getItemMeta().equals(this.items.getCitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Cocaine")) {
                                total = total + this.main.getConfig().getDouble("Selling.Cocaine") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Cocaine")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getCpitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.CocaPlant")) {
                                total = total + this.main.getConfig().getDouble("Selling.CocaPlant") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Heroin")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getHitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Heroin")) {
                                total = total + this.main.getConfig().getDouble("Selling.Heroin") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Heroin")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getOpitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.OpiumPoppy")) {
                                total = total + this.main.getConfig().getDouble("Selling.OpiumPoppy") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Opium Poppy")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getMethmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Meth")) {
                                total = total + this.main.getConfig().getDouble("Selling.Meth") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Meth")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getEsmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Ephedra")) {
                                total = total + this.main.getConfig().getDouble("Selling.Ephedra") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Ephedra")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getWitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Weed")) {
                                total = total + this.main.getConfig().getDouble("Selling.Weed") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Weed")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getWsitemmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.WeedPlant")) {
                                total = total + this.main.getConfig().getDouble("Selling.WeedPlant") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Weed Plant")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getShroomMeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Shrooms")) {
                                total = total + this.main.getConfig().getDouble("Selling.Shrooms") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Shrooms")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getEcmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.Ecstacy")) {
                                total = total + this.main.getConfig().getDouble("Selling.Ecstacy") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Ecstacy")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.items.getSsmeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.SassafrasSapling")) {
                                total = total + this.main.getConfig().getDouble("Selling.SassafrasSapling") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "SassafrasSapling")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.lsd.getAcidMeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.LSD")) {
                                total = total + this.main.getConfig().getDouble("Selling.LSD") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "LSD")), this.main));
                            }
                        } else if (i.getItemMeta().equals(this.lsd.getMorningGloryPlantMeta())) {
                            if (this.main.getConfig().getBoolean("enableSelling.MorningGloryPlant")) {
                                total = total + this.main.getConfig().getDouble("Selling.MorningGloryPlant") * (double)i.getAmount();
                                i.setAmount(0);
                            } else {
                                e.getWhoClicked().sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToSell").replace("%drugs%", "Morning Glory Plant")), this.main));
                            }
                        } else if (i != null && i.getType() != Material.EMERALD_BLOCK && i.getType() != Material.BARRIER) {
                            e.getWhoClicked().getWorld().dropItemNaturally(e.getWhoClicked().getLocation(), i);
                            e.getClickedInventory().remove(i);
                        }
                    }
                }

                if (Boolean.TRUE) {
                    Main.economy.depositPlayer(e.getWhoClicked().getName(), total);
                    if (total != 0.0D) {
                        e.getWhoClicked().sendMessage(Texts.prefix + ChatColor.GREEN + "+ $" + ChatColor.GREEN + total);
                    } else {
                        e.getWhoClicked().sendMessage(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("noItemsToSell")), this.main));
                    }
                }
            }

            if (e.getSlot() == 53) {
                e.setCancelled(true);
                e.getWhoClicked().closeInventory();
                this.openMainMenu((Player)e.getWhoClicked());
            }
        }

        if (e.getView().getTitle().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("BuyMenuTitle")), this.main))) {
            Player p = (Player)e.getWhoClicked();
            Double cocainebuy = this.main.getConfig().getDouble("Buying.Cocaine");
            Double cocainemissing = cocainebuy - Main.economy.getBalance(p.getName());
            Double heroinbuy = this.main.getConfig().getDouble("Buying.Heroin");
            Double heroinmissing = heroinbuy - Main.economy.getBalance(p.getName());
            Double weedbuy = this.main.getConfig().getDouble("Buying.Weed");
            Double weedmissing = weedbuy - Main.economy.getBalance(p.getName());
            Double methbuy = this.main.getConfig().getDouble("Buying.Meth");
            Double methmissing = methbuy - Main.economy.getBalance(p.getName());
            Double ShroomsBuy = this.main.getConfig().getDouble("Buying.Shrooms");
            Double ShroomsMissing = ShroomsBuy - Main.economy.getBalance(p.getName());
            Double CocaPlantBuy = this.main.getConfig().getDouble("Buying.CocaPlant");
            Double CocaPlantMissing = CocaPlantBuy - Main.economy.getBalance(p.getName());
            Double OpiumPoppyBuy = this.main.getConfig().getDouble("Buying.OpiumPoppy");
            Double OpiumPoppyMissing = OpiumPoppyBuy - Main.economy.getBalance(p.getName());
            Double EphedraBuy = this.main.getConfig().getDouble("Buying.Ephedra");
            Double EphedraMissing = EphedraBuy - Main.economy.getBalance(p.getName());
            Double WeedPlantBuy = this.main.getConfig().getDouble("Buying.WeedPlant");
            Double WeedPlantMissing = WeedPlantBuy - Main.economy.getBalance(p.getName());
            Double EcstacyBuy = this.main.getConfig().getDouble("Buying.Ecstacy");
            Double EcstacyMissing = EcstacyBuy - Main.economy.getBalance(p.getName());
            Double SassafrasPlantBuy = this.main.getConfig().getDouble("Buying.SassafrasSapling");
            Double SassafrasPlantMissing = SassafrasPlantBuy - Main.economy.getBalance(p.getName());
            Double LSDBuy = this.main.getConfig().getDouble("Buying.LSD");
            Double LSDMissing = LSDBuy - Main.economy.getBalance(p.getName());
            Double MorningGloryPlantBuy = this.main.getConfig().getDouble("Buying.MorningGloryPlant");
            Double MorningGloryPlantMissing = MorningGloryPlantBuy - Main.economy.getBalance(p.getName());
            if (e.getCurrentItem() != null && e.getCurrentItem().getType() != null) {
                if (e.getRawSlot() != 26 && !HexChat.translateHexCodes((String)e.getCurrentItem().getItemMeta().getLore().get(0), this.main).equals(ChatColor.RED + "NOT FOR SALE")) {
                    switch(e.getRawSlot()) {
                        case 0:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= cocainebuy) {
                                Main.economy.withdrawPlayer(p.getName(), cocainebuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getCitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getCitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Cocaine for $" + ChatColor.RED + cocainebuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + cocainemissing);
                            }
                            break;
                        case 1:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= heroinbuy) {
                                Main.economy.withdrawPlayer(p.getName(), heroinbuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getHitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getHitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Heroin for $" + ChatColor.RED + heroinbuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + heroinmissing);
                            }
                            break;
                        case 2:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= weedbuy) {
                                Main.economy.withdrawPlayer(p.getName(), weedbuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getWitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getWitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Weed for $" + ChatColor.RED + weedbuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + weedmissing);
                            }
                            break;
                        case 3:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= methbuy) {
                                Main.economy.withdrawPlayer(p.getName(), methbuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getMethitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getMethitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Meth for $" + ChatColor.RED + methbuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + methmissing);
                            }
                            break;
                        case 4:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= ShroomsBuy) {
                                Main.economy.withdrawPlayer(p.getName(), ShroomsBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getShroomItem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getShroomItem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Shrooms for $" + ChatColor.RED + ShroomsBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + ShroomsMissing);
                            }
                            break;
                        case 5:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= EcstacyBuy) {
                                Main.economy.withdrawPlayer(p.getName(), EcstacyBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getEcitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getEcitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Ecstacy for $" + ChatColor.RED + EcstacyBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + EcstacyMissing);
                            }
                            break;
                        case 6:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= LSDBuy) {
                                Main.economy.withdrawPlayer(p.getName(), LSDBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.lsd.getAcid(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.lsd.getAcid(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased LSD for $" + ChatColor.RED + LSDBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + LSDMissing);
                            }
                        case 7:
                        case 8:
                        case 13:
                        default:
                            break;
                        case 9:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= CocaPlantBuy) {
                                Main.economy.withdrawPlayer(p.getName(), CocaPlantBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getCpitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getCpitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Coca Plant for $" + ChatColor.RED + CocaPlantBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + CocaPlantMissing);
                            }
                            break;
                        case 10:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= OpiumPoppyBuy) {
                                Main.economy.withdrawPlayer(p.getName(), OpiumPoppyBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getOpitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getOpitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Opium Poppy for $" + ChatColor.RED + OpiumPoppyBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + OpiumPoppyMissing);
                            }
                            break;
                        case 11:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= WeedPlantBuy) {
                                Main.economy.withdrawPlayer(p.getName(), WeedPlantBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getWsitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getWsitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Weed Plant for $" + ChatColor.RED + WeedPlantBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + WeedPlantMissing);
                            }
                            break;
                        case 12:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= EphedraBuy) {
                                Main.economy.withdrawPlayer(p.getName(), EphedraBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getEsitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getEsitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Ephedra for $" + ChatColor.RED + EphedraBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + EphedraMissing);
                            }
                            break;
                        case 14:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= SassafrasPlantBuy) {
                                Main.economy.withdrawPlayer(p.getName(), SassafrasPlantBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.items.getSsitem(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.items.getSsitem(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Sassafras Plant for $" + ChatColor.RED + SassafrasPlantBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + SassafrasPlantMissing);
                            }
                            break;
                        case 15:
                            e.setCancelled(true);
                            if (Main.economy.getBalance(p.getName()) >= MorningGloryPlantBuy) {
                                Main.economy.withdrawPlayer(p.getName(), MorningGloryPlantBuy);
                                if (!this.isFullInv(p)) {
                                    p.getInventory().addItem(new ItemStack[]{this.lsd.getMorningGloryPlant(1)});
                                } else {
                                    p.getWorld().dropItemNaturally(p.getLocation(), this.lsd.getMorningGloryPlant(1));
                                }

                                p.sendMessage(Texts.prefix + ChatColor.RED + "You have purchased Morning Glory Plant for $" + ChatColor.RED + MorningGloryPlantBuy);
                            } else {
                                p.sendMessage(Texts.prefix + ChatColor.RED + "Missing $" + ChatColor.RED + MorningGloryPlantMissing);
                            }
                    }
                }

                if (e.getSlot() == 26) {
                    e.setCancelled(true);
                    e.getWhoClicked().closeInventory();
                    this.openMainMenu((Player)e.getWhoClicked());
                    return;
                }

                if (HexChat.translateHexCodes((String)e.getCurrentItem().getItemMeta().getLore().get(0), this.main).equals(ChatColor.RED + "NOT FOR SALE")) {
                    e.setCancelled(true);
                    p.sendMessage(Texts.prefix + HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("unableToBuy").replace("%drugs%", e.getCurrentItem().getItemMeta().getDisplayName())), this.main));
                }
            }
        }

    }

    public boolean isFullInv(Player p) {
        return p.getInventory().firstEmpty() == -1;
    }

    @EventHandler
    public void ItemDrops(InventoryCloseEvent e) {
        if (e.getView().getTitle().equals(HexChat.translateHexCodes(Objects.requireNonNull(this.main.getConfig().getString("SellMenuTitle")), this.main))) {
            Iterator var3 = e.getInventory().iterator();

            while(var3.hasNext()) {
                ItemStack item = (ItemStack)var3.next();
                if (item != null && item.getType() != Material.EMERALD_BLOCK && item.getType() != Material.BARRIER) {
                    e.getPlayer().getWorld().dropItemNaturally(e.getPlayer().getLocation(), item);
                }
            }
        }

    }
}