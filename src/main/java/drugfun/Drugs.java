package drugfun;

import java.util.UUID;
import org.bukkit.entity.Player;

public enum Drugs {
    COCAINE,
    ECSTASY,
    LSD,
    HEROIN,
    WEED,
    METH,
    SHROOMS;

    // $FF: synthetic field
    private static int[] $SWITCH_TABLE$drugfun$Drugs;

    static Boolean isOverdose(Player p, Drugs drugs) {
        UUID uuid = p.getUniqueId();
        switch($SWITCH_TABLE$drugfun$Drugs()[drugs.ordinal()]) {
            case 1:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getCocaine() >= Main.odcocaine) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            case 2:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getEcstasy() >= Main.odecstasy) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            case 3:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getLsd() >= Main.odlsd) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            case 4:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getHeroin() >= Main.odheroin) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            case 5:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getWeed() >= Main.odweed) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            case 6:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getMeth() >= Main.odmeth) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                }

                return false;
            default:
                if (((DrugBloodManager)Main.bloods.get(uuid)).getShrooms() >= Main.odshrooms) {
                    p.sendMessage(Texts.prefix + Main.overdoseMsg);
                    p.setHealth(0.0D);
                    Main.clearBlood(p);
                    return true;
                } else {
                    return false;
                }
        }
    }

    // $FF: synthetic method
    static int[] $SWITCH_TABLE$drugfun$Drugs() {
        int[] var10000 = $SWITCH_TABLE$drugfun$Drugs;
        if (var10000 != null) {
            return var10000;
        } else {
            int[] var0 = new int[values().length];

            try {
                var0[COCAINE.ordinal()] = 1;
            } catch (NoSuchFieldError var7) {
            }

            try {
                var0[ECSTASY.ordinal()] = 2;
            } catch (NoSuchFieldError var6) {
            }

            try {
                var0[HEROIN.ordinal()] = 4;
            } catch (NoSuchFieldError var5) {
            }

            try {
                var0[LSD.ordinal()] = 3;
            } catch (NoSuchFieldError var4) {
            }

            try {
                var0[METH.ordinal()] = 6;
            } catch (NoSuchFieldError var3) {
            }

            try {
                var0[SHROOMS.ordinal()] = 7;
            } catch (NoSuchFieldError var2) {
            }

            try {
                var0[WEED.ordinal()] = 5;
            } catch (NoSuchFieldError var1) {
            }

            $SWITCH_TABLE$drugfun$Drugs = var0;
            return var0;
        }
    }
}
