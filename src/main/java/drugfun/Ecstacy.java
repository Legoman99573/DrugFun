package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.Bisected;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class Ecstacy implements Listener {
    private static Main main;
    public ArrayList<Player> HCooldown = new ArrayList();
    Items i = new Items();
    Random r = new Random();
    ArrayList<Integer> mercuryDropped = new ArrayList();

    public Ecstacy(Main main) {
        Ecstacy.main = main;
        this.mercuryDrop();
    }

    @EventHandler
    public void EcstacySwallow(final PlayerInteractEvent e10) {
        if (e10.getAction().equals(Action.RIGHT_CLICK_AIR) || e10.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            final Player pl = e10.getPlayer();
            if (pl.getItemInHand().hasItemMeta() && Objects.requireNonNull(pl.getItemInHand().getItemMeta()).hasLore() && HexChat.translateHexCodes( (String)pl.getItemInHand().getItemMeta().getLore().get(0), main).equals(ChatColor.WHITE + "Ecstasy or molly, is a psychoactive drug ")) {
                if (!Objects.equals(e10.getHand(), EquipmentSlot.HAND)) {
                    return;
                }

                if (!e10.getPlayer().hasPermission("drugfun.consumeecstasy")) {
                    e10.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Ecstasy")), main));
                    e10.setCancelled(true);
                    return;
                }

                if (this.HCooldown.contains(e10.getPlayer())) {
                    return;
                }

                e10.setCancelled(true);
                pl.getInventory().getItemInHand().setAmount(pl.getInventory().getItemInHand().getAmount() - 1);
                pl.addPotionEffects(EffectsManager.EcstacyEffects);
                SlurManager.addSlurFinish(e10.getPlayer(), System.currentTimeMillis() + (long)(Main.ecstasySlurTime * 1000.0D));
                SlurManager.setEffect(e10.getPlayer(), Main.ecstasySlur);
                if (main.cs) {
                    pl.playSound(pl.getLocation(), Sound.ENTITY_GENERIC_EAT, 1.0F, 0.7F);
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        if (Ecstacy.main.acs) {
                            pl.playSound(pl.getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                        }

                    }
                }, 8L);
                pl.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("EsctacySwallow")), main));
                Drugs.isOverdose(e10.getPlayer(), Drugs.ECSTASY);
                this.HCooldown.add(e10.getPlayer());
                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        Ecstacy.this.HCooldown.remove(e10.getPlayer());
                    }
                }, 20L);
            }
        }

    }

    @EventHandler
    public void onSassafrasPlant(BlockPlaceEvent e) {
        if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta() && e.getPlayer().getInventory().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getInventory().getItemInHand().getItemMeta(), this.i.getSsmeta())) {
            FileManager.putLocation(e.getBlock().getLocation(), 0L);
            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("SassafrasPlant")), main));
            if (main.dps) {
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }

    }

    @EventHandler
    public void SassafrasGrow(StructureGrowEvent e) {
        if ((e.getSpecies().equals(TreeType.TREE)) && FileManager.isValidLocation(e.getLocation())) {
            Iterator var3 = e.getBlocks().iterator();

            while(var3.hasNext()) {
                BlockState b = (BlockState)var3.next();
                FileManager.putLocation(b.getLocation(), 0L);
                FileManager.removeLocation(b.getLocation().add(0.0D, -2.0D, 0.0D));
            }
        }
        if ((e.getSpecies().equals(TreeType.BIG_TREE)) && FileManager.isValidLocation(e.getLocation())) {
            e.setCancelled(true);
            // Bukkit.broadcastMessage(Texts.prefix + ChatColor.RED + "Detected Large Tree. Cancelling StructureGrowEvent at x: " + e.getLocation().getBlockX() + " y: " + e.getLocation().getBlockY() + " z: " + e.getLocation().getBlockZ());
        }
    }

    @EventHandler
    public void HarvestSassafras(BlockBreakEvent e) {
        Random r = new Random();
        if (FileManager.isValidLocation(e.getBlock().getLocation())) {
            if (e.getBlock().getType() == Material.OAK_LOG) {
                FileManager.removeLocation(e.getBlock().getLocation());
                e.setDropItems(false);
                Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSbark(main.SbarkAmount));
                e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("SassafrasBarkHarvest")), main));
            }

            if (e.getBlock().getType() == Material.OAK_LEAVES) {
                FileManager.removeLocation(e.getBlock().getLocation());
                e.setDropItems(false);
                if (((ItemStack)Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                    Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                }
            }

            if (e.getBlock().getType() == Material.OAK_SAPLING) {
                FileManager.removeLocation(e.getBlock().getLocation());
                e.setDropItems(false);
                Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
            }
        }
    }


    @EventHandler
    public void onSafroleCancel(BlockPlaceEvent e) {
        if (e.getPlayer().getInventory().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getInventory().getItemInHand().getItemMeta(), this.i.getSafroleMeta())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onMercuryDrop(BlockBreakEvent e) {
        if (e.getBlock().getLocation().add(0.0D, -11.0D, 0.0D).getBlock().getType().equals(Material.VOID_AIR) && e.getBlock().getType().equals(Material.STONE) && ((ItemStack)Main.mercuryList.get(this.r.nextInt(Main.mercuryList.size() - 1))).getType() == Material.DIAMOND) {
            Integer amt = (Integer)this.mercuryDropped.get(this.r.nextInt(this.mercuryDropped.size() - 1));
            Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getMercury(amt));
        }

    }

    public void mercuryDrop() {
        ArrayList<String> split = new ArrayList(Arrays.asList(main.mercuryAmount.split(",")));

        for(int i = Integer.parseInt((String)split.get(0)); i < Integer.parseInt((String)split.get(1)) + 1; ++i) {
            this.mercuryDropped.add(i);
        }

    }

    @EventHandler
    public void onSaplingDecay(LeavesDecayEvent e) {
        Block block = e.getBlock();
        if (FileManager.isValidLocation(e.getBlock().getLocation())) {
            FileManager.removeLocation(block.getLocation());
            e.setCancelled(true);
            block.setType(Material.AIR);
            if (((ItemStack)Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
            }
        }
    }

    @EventHandler
    public void SassafrasPlantWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.OAK_SAPLING && FileManager.isValidLocation(block.getLocation())) {
            FileManager.removeLocation(block.getLocation());
            e.setCancelled(true);
            block.setType(Material.AIR);
            Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
        }
    }

    @EventHandler
    public void SassafrasExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block) var2.next();
            if (block.getType() == Material.OAK_SAPLING) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
                }
            }
            if (block.getType() == Material.OAK_LOG) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSbark(main.SbarkAmount));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSbark(main.SbarkAmount));
                }
            }
            if (block.getType() == Material.OAK_LEAVES) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    if (((ItemStack) Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                        Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                    }

                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    if (((ItemStack) Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                        Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                    }
                }
            }
        }
    }

    @EventHandler
    public void SassafrasRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.OAK_SAPLING) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.i.getSsitem(1));
                }
            }
            if (block.getType() == Material.OAK_LOG) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSbark(main.SbarkAmount));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSbark(main.SbarkAmount));
                }
            }
            if (block.getType() == Material.OAK_LEAVES) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    if (((ItemStack)Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                        Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                    }

                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    FileManager.removeLocation(block.getLocation());
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    if (((ItemStack)Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                        Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                    }
                }
            }
        }
    }

    @EventHandler
    public void SassafrasPlantEntityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.OAK_SAPLING && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getSsitem(1));
            }
        }

    }

    @EventHandler
    public void SassafrasPlantLogExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.OAK_LOG && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(e.getLocation().getWorld()).dropItemNaturally(e.getLocation(), this.i.getSbark(main.SbarkAmount));
            }
        }

    }

    @EventHandler
    public void SassafrasPlantLeavesExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.OAK_LEAVES && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                if (((ItemStack)Main.sassafrasList.get(r.nextInt(Main.sassafrasList.size() - 1))).getType() == Material.DIAMOND) {
                    Objects.requireNonNull(e.getLocation().getWorld()).dropItemNaturally(e.getLocation(), this.i.getSsitem(main.sassafrasplantAmount));
                }
            }
        }
    }
}
