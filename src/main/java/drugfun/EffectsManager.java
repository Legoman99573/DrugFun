package drugfun;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EffectsManager {
    public static ArrayList<String> CocaineEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> CocaineEffects = new ArrayList();
    public static ArrayList<String> HeroinEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> HeroinEffects = new ArrayList();
    public static ArrayList<String> WeedEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> WeedEffects = new ArrayList();
    public static ArrayList<String> MethEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> MethEffects = new ArrayList();
    public static ArrayList<String> ShroomsEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> ShroomsEffects = new ArrayList();
    public static ArrayList<String> EcstacyEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> EcstacyEffects = new ArrayList();
    public static ArrayList<String> LSDEffectsRaw = new ArrayList();
    public static ArrayList<PotionEffect> LSDEffects = new ArrayList();

    EffectsManager(Main main) {
        CocaineEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Cocaine");
        HeroinEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Heroin");
        WeedEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Weed");
        MethEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Meth");
        ShroomsEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Shrooms");
        EcstacyEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.Ecstacy");
        LSDEffectsRaw = (ArrayList)main.getConfig().getStringList("DrugEffects.LSD");
        this.rawToEffects();
    }

    public void rawToEffects() {
        Iterator var2 = CocaineEffectsRaw.iterator();

        String s;
        ArrayList split;
        String var4;
        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            CocaineEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = LSDEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            LSDEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = HeroinEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            HeroinEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = WeedEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            WeedEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = MethEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            MethEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = ShroomsEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            ShroomsEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

        var2 = EcstacyEffectsRaw.iterator();

        while(var2.hasNext()) {
            s = (String)var2.next();
            split = new ArrayList(Arrays.asList(s.split(",")));
            if (split.size() == 3) {
                switch((var4 = ((String)split.get(0)).toUpperCase()).hashCode()) {
                    case -1929420024:
                        if (var4.equals("POISON")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.POISON, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1892419057:
                        if (var4.equals("NIGHT_VISION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1833148097:
                        if (var4.equals("SLOW_DIGGING")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1787106870:
                        if (var4.equals("UNLUCK")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.UNLUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1734240269:
                        if (var4.equals("WITHER")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.WITHER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1721024447:
                        if (var4.equals("STRENGTH")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1356753140:
                        if (var4.equals("BLINDNESS")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.BLINDNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -1343491295:
                        if (var4.equals("BAD_OMEN")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.BAD_OMEN, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -944915573:
                        if (var4.equals("REGENERATION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.REGENERATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -774622513:
                        if (var4.equals("ABSORPTION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -282407383:
                        if (var4.equals("SLOW_FALLING")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case -216510496:
                        if (var4.equals("HEALTH_BOOST")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2210036:
                        if (var4.equals("HARM")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.HARM, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2213352:
                        if (var4.equals("HEAL")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.HEAL, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2288686:
                        if (var4.equals("JUMP")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.JUMP, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2347953:
                        if (var4.equals("LUCK")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.LUCK, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2548225:
                        if (var4.equals("SLOW")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.SLOW, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 46439887:
                        if (var4.equals("WEAKNESS")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.WEAKNESS, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 68512363:
                        if (var4.equals("HASTE")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 79104039:
                        if (var4.equals("SPEED")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.SPEED, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 178114541:
                        if (var4.equals("LEVITATION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.LEVITATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 254601170:
                        if (var4.equals("SATURATION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.SATURATION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 428830473:
                        if (var4.equals("DAMAGE_RESISTANCE")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 536276471:
                        if (var4.equals("INVISIBILITY")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 839690005:
                        if (var4.equals("GLOWING")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.GLOWING, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1073139170:
                        if (var4.equals("FIRE_RESISTANCE")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 1993593830:
                        if (var4.equals("CONFUSION")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.CONFUSION, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                        break;
                    case 2142192307:
                        if (var4.equals("HUNGER")) {
                            EcstacyEffects.add(new PotionEffect(PotionEffectType.HUNGER, Integer.parseInt((String)split.get(1)), Integer.parseInt((String)split.get(2))));
                        }
                }
            }
        }

    }
}