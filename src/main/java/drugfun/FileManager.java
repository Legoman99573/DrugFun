package drugfun;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {
    private static File file;
    private static YamlConfiguration config;
    Integer seconds;

    FileManager(Main main) {
        file = new File(main.getDataFolder(), "locations.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException var3) {
                var3.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(file);
        this.seconds = main.getConfig().getInt("locations-update-interval");
    }
    public static void saveOnDisable() {
        Bukkit.getLogger().info(Texts.prefix + ChatColor.RED + "Attempting to save locations.yml");
        try {
            config.save(file);
            Bukkit.getLogger().info(Texts.prefix + ChatColor.GREEN + "Successfully saved locations.yml.");
        } catch (IOException var2) {
            Bukkit.getLogger().severe(Texts.prefix + ChatColor.DARK_RED + "Failed to save locations.yml. Any drugs that were in memory will not be available once the server is started back up.");
            var2.printStackTrace();
        }
    }

    public static void saveOnForceSave() {
        Bukkit.getLogger().info(Texts.prefix + ChatColor.RED + "Attempting to force save locations.yml");
        try {
            Bukkit.getLogger().info(Texts.prefix + ChatColor.GREEN + "Successfully force saved locations.yml.");
            config.save(file);
        } catch (IOException var2) {
            Bukkit.getLogger().severe(Texts.prefix + ChatColor.DARK_RED + "Failed to force save locations.yml. Check your server's Disk Space and make sure that locations.yml is writable.");
            var2.printStackTrace();
        }
    }

    private static String getStringFromLocation(Location location) {
        if (location.getWorld() != null) {
            return location.getWorld().getName() + " " + location.getBlockX() + " " + location.getBlockY() + " " + location.getBlockZ();
        } else {
            Bukkit.getLogger().severe("Failed to get string from location x:" + location.getBlockX() + " y:" + location.getBlockY() + " z:" + location.getBlockZ());
            return null;
        }
    }

    public void onSaveDataReapeating(Main main) {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
            public void run() {
                Bukkit.getLogger().info(Texts.prefix + ChatColor.RED + "Attempting to save locations.yml");
                try {
                    FileManager.config.save(FileManager.file);
                    Bukkit.getLogger().info(Texts.prefix + ChatColor.GREEN + "Successfully saved locations.yml. Next scheduled save in " + seconds + " seconds");
                } catch (IOException var2) {
                    Bukkit.getLogger().severe(Texts.prefix + ChatColor.DARK_RED + "Failed to save locations.yml. The data is still cached in memory, but will be lost upon server crash or stopping the server. Try using /df forcesave as sometimes this can happen.");
                    var2.printStackTrace();
                }
            }
        }, (long)(seconds * 20), (long)(seconds * 20));
    }

    public static void putLocation(Location l, long t) {
        config.set(Objects.requireNonNull(getStringFromLocation(l)), t);
    }

    public static boolean isValidLocation(Location l) {
        return config.contains(Objects.requireNonNull(getStringFromLocation(l)));
    }

    public static Long getLocationTime(Location l) {
        return config.getLong(Objects.requireNonNull(getStringFromLocation(l)));
    }

    public static void setTime(Location location, Long time) {
        config.set(Objects.requireNonNull(getStringFromLocation(location)), time);
    }

    public static void removeLocation(Location l) {
        config.set(Objects.requireNonNull(getStringFromLocation(l)), (Object)null);
        if (l.getBlock().getType() == Material.TALL_GRASS || l.getBlock().getType() == Material.LARGE_FERN) {
            if (config.contains(Objects.requireNonNull(getStringFromLocation(l.add(0.0D, 1.0D, 0.0D))))) {
                config.set(Objects.requireNonNull(getStringFromLocation(l)), (Object)null);
            }

            if (config.contains(Objects.requireNonNull(getStringFromLocation(l.add(0.0D, -2.0D, 0.0D))))) {
                config.set(Objects.requireNonNull(getStringFromLocation(l)), (Object)null);
            }
        }

    }
}
