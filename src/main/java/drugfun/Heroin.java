package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

public class Heroin implements Listener {
    private static Main main;
    Items i = new Items();
    public ArrayList<Player> HCooldown = new ArrayList();
    public ArrayList<Player> HSyrCooldown = new ArrayList();
    HashMap<Location, Long> canHarvest = new HashMap();
    Items items = new Items();
    Random r = new Random();

    public Heroin(Main main) {
        Heroin.main = main;
    }

    @EventHandler
    public void SyringeUseHeroin(final PlayerInteractEvent e) {
        if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && Objects.equals(e.getHand(), EquipmentSlot.HAND) && e.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getItemInHand().getItemMeta(), this.i.getSyrHeroinMeta())) {
            if (!e.getPlayer().hasPermission("drugfun.consumeheroin")) {
                e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Heroin")), main));
                e.setCancelled(true);
                return;
            }

            if (this.HSyrCooldown.contains(e.getPlayer())) {
                return;
            }

            e.setCancelled(true);
            e.getPlayer().addPotionEffects(EffectsManager.HeroinEffects);
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
            e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("selfInjectHeroin")), main));
            if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setHeroin(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getHeroin() + 10);
            }

            SlurManager.addSlurFinish(e.getPlayer(), System.currentTimeMillis() + (long)(Main.heroinSlurTime * 1000.0D));
            SlurManager.setEffect(e.getPlayer(), Main.heroinSlur);
            Drugs.isOverdose(e.getPlayer(), Drugs.HEROIN);
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    if (Heroin.main.acs) {
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                    }

                }
            }, 8L);
            this.HSyrCooldown.add(e.getPlayer());
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    Heroin.this.HSyrCooldown.remove(e.getPlayer());
                }
            }, 20L);
        }

    }

    @EventHandler
    public void InjectIntoEntitesH(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
            Player p = (Player)e.getDamager();
            LivingEntity et = (LivingEntity)e.getEntity();
            if (p.getInventory().getItemInHand().hasItemMeta() && Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.i.getSyrHeroinMeta())) {
                if (!p.hasPermission("drugfun.consumeheroin")) {
                    p.sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Heroin")), main));
                    e.setCancelled(true);
                    return;
                }

                p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
                e.setCancelled(true);
                p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
                et.addPotionEffects(EffectsManager.HeroinEffects);
                p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("PlayerInjectPlayerHeroinAttacker")), main));
                et.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("PlayerInjectPlayerHeroinVictim")), main));
                if (Main.bloods.containsKey(et.getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(et.getUniqueId())).setHeroin(((DrugBloodManager)Main.bloods.get(et.getUniqueId())).getHeroin() + 10);
                }

                Drugs.isOverdose((Player)et, Drugs.HEROIN);
                SlurManager.addSlurFinish((Player)et, System.currentTimeMillis() + (long)(Main.heroinSlurTime * 1000.0D));
                SlurManager.setEffect((Player)et, Main.heroinSlur);
            }
        }

    }

    @EventHandler
    public void PoppyPlant(BlockPlaceEvent e1) {
        if (e1.getPlayer().getItemInHand() != null && e1.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e1.getPlayer().getItemInHand().getItemMeta(), this.i.getOpitemmeta())) {
            e1.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HeroinPlant")), main));
            FileManager.putLocation(e1.getBlock().getLocation(), (long)((double)System.currentTimeMillis() + main.HeroinTime * 60.0D * 1000.0D));
            if (main.dps) {
                e1.getPlayer().playSound(e1.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }

    }

    static void OpiumRecipe() {
        Items i = new Items();
        FurnaceRecipe recipe = new FurnaceRecipe(i.getOitem(2), Material.LIME_DYE);
        recipe.setExperience(50.0F);
        main.getServer().addRecipe(recipe);
    }

    @EventHandler
    public void HeroinSnort(final PlayerInteractEvent e10) {
        if (e10.getAction().equals(Action.RIGHT_CLICK_AIR) || e10.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            final Player pl = e10.getPlayer();
            if (pl.getItemInHand().hasItemMeta() && Objects.equals(pl.getItemInHand().getItemMeta(), this.i.getHitemmeta())) {
                if (!e10.getHand().equals(EquipmentSlot.HAND)) {
                    return;
                }

                if (!e10.getPlayer().hasPermission("drugfun.consumeheroin")) {
                    e10.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Heroin")), main));
                    e10.setCancelled(true);
                    return;
                }

                if (this.HCooldown.contains(e10.getPlayer())) {
                    return;
                }

                e10.setCancelled(true);
                pl.getInventory().getItemInHand().setAmount(pl.getInventory().getItemInHand().getAmount() - 1);
                pl.addPotionEffects(EffectsManager.HeroinEffects);
                if (main.cs) {
                    pl.playSound(pl.getLocation(), Sound.ENTITY_GENERIC_DRINK, 1.0F, 0.7F);
                }

                if (Main.bloods.containsKey(e10.getPlayer().getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(e10.getPlayer().getUniqueId())).setHeroin(((DrugBloodManager)Main.bloods.get(e10.getPlayer().getUniqueId())).getHeroin() + 10);
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        if (Heroin.main.acs) {
                            pl.playSound(pl.getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                        }

                    }
                }, 8L);
                pl.sendMessage(Texts.prefix + HexChat.translateHexCodes( main.getConfig().getString("SnortHeroin"), main));
                Drugs.isOverdose(e10.getPlayer(), Drugs.HEROIN);
                this.HCooldown.add(e10.getPlayer());
                SlurManager.addSlurFinish(e10.getPlayer(), System.currentTimeMillis() + (long)(Main.heroinSlurTime * 1000.0D));
                SlurManager.setEffect(e10.getPlayer(), Main.heroinSlur);
                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        Heroin.this.HCooldown.remove(e10.getPlayer());
                    }
                }, 20L);
            }
        }

    }

    @EventHandler
    public void HarvestOpiumSeeds(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getHand() != EquipmentSlot.HAND) {
                return;
            }

            if (e.getClickedBlock().getType() != null && e.getClickedBlock().getType().equals(Material.ALLIUM) && FileManager.isValidLocation(e.getClickedBlock().getLocation())) {
                if (System.currentTimeMillis() >= FileManager.getLocationTime(e.getClickedBlock().getLocation())) {
                    Objects.requireNonNull(e.getClickedBlock().getLocation().getWorld()).dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getOpspitem(main.HeroinSeedsAmount));
                    if (((ItemStack)Main.heroinList.get(this.r.nextInt(Main.heroinList.size() - 1))).getType() == Material.DIAMOND) {
                        e.getClickedBlock().getLocation().getWorld().dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getOpitem(main.heroinplantAmount));
                    }

                    FileManager.setTime(e.getClickedBlock().getLocation(), (long)((double)System.currentTimeMillis() + main.HeroinTime * 60.0D * 1000.0D));
                    e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HeroinPlantHarvest")), main));
                    if (main.dhs) {
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.7F);
                    }
                } else {
                    e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HeroinPlantHarvestFail")), main));
                }
            }
        }

    }

    @EventHandler
    public void receiveOpiumPoppy(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.ALLIUM && FileManager.isValidLocation(e.getBlock().getLocation())) {
            e.setDropItems(false);
            Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getOpitem(1));
            FileManager.removeLocation(e.getBlock().getLocation());
        }

    }

    @EventHandler
    public void OpiumPoppyWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.ALLIUM && FileManager.isValidLocation(block.getLocation())) {
            e.setCancelled(true);
            block.setType(Material.AIR);
            FileManager.removeLocation(block.getLocation());
            Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
        }

    }

    @EventHandler
    public void OpiumPoppyExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.ALLIUM) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
                }
            }
        }

    }

    @EventHandler
    public void OpiumPoppyRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.ALLIUM) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
                }
            }
        }

    }

    @EventHandler
    public void OpiumPoppyEntityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.ALLIUM && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getOpitem(1));
            }
        }

    }
}
