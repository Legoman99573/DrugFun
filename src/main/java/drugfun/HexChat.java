package drugfun;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.md_5.bungee.api.ChatColor;

public class HexChat {
    private static final Pattern HEX_PATTERN = Pattern.compile("&#([A-Fa-f0-9]{6})");
    private static final char COLOR_CHAR = '§';

    public static String translateHexCodes(String message, Main plugin) {
        return translate(HEX_PATTERN, message, plugin);
    }

    public static String translateHexCodes(String startTag, String endTag, String message, Main plugin) {
        Pattern hexPattern = Pattern.compile(startTag + "([a-f0-9]{6})" + endTag);
        return translate(hexPattern, message, plugin);
    }

    private static String translate(Pattern hex, String message, Main plugin) {
        if (Main.getServerVersion().getVersionInt() < VersionEnums.VERSION_116.getVersionInt()) {
            return ChatColor.translateAlternateColorCodes('&', message);
        } else {
            Matcher matcher = hex.matcher(message);
            StringBuffer buffer = new StringBuffer(message.length() + 32);
            while (matcher.find()) {
                String group = matcher.group(1);
                matcher.appendReplacement(buffer, "§x§" + group.charAt(0) + '§' + group.charAt(1) + '§' + group.charAt(2) + '§' + group.charAt(3) + '§' + group.charAt(4) + '§' + group.charAt(5));
            }
            return ChatColor.translateAlternateColorCodes('&', matcher.appendTail(buffer).toString());
        }
    }
}
