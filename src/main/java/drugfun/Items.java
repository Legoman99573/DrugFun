package drugfun;

import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

class Items {
    private ItemStack citem;
    private ItemMeta citemmeta;
    private ItemStack clitem;
    private ItemMeta clitemmeta;
    private ItemStack cpitem;
    private ItemMeta cpitemmeta;
    private ItemStack hitem;
    private ItemMeta hitemmeta;
    private ItemStack mitem;
    private ItemMeta mmmeta;
    private ItemStack oitem;
    private ItemMeta oitemmeta;
    private ItemStack opitem;
    private ItemMeta opitemmeta;
    private ItemStack opspitem;
    private ItemMeta opspitemmeta;
    private ItemStack Ecitem;
    private ItemMeta Ecmeta;
    private ItemStack witem;
    private ItemMeta witemmeta;
    private ItemStack HashItem;
    private ItemMeta HashMeta;
    private ItemStack wsitem;
    private ItemMeta wsitemmeta;
    private ItemStack bluntitem;
    private ItemMeta bluntitemmeta;
    private ItemStack LitBlunt;
    private ItemMeta litbluntitem;
    private ItemStack methitem;
    private ItemMeta methmeta;
    private ItemStack hclitem;
    private ItemMeta hclmeta;
    private ItemStack pitem;
    private ItemMeta pmeta;
    private ItemStack iitem;
    private ItemMeta imeta;
    private ItemStack eitem;
    private ItemMeta emeta;
    private ItemStack esitem;
    private ItemMeta esmeta;
    private ItemStack ssitem;
    private ItemMeta ssmeta;
    private ItemStack SyrItem;
    private ItemMeta SyrMeta;
    private ItemMeta SyrCocaineMeta;
    private ItemStack SyrCocaineItem;
    private ItemStack SyrHeroinItem;
    private ItemMeta SyrHeroinMeta;
    private ItemStack SyrMethItem;
    private ItemMeta SyrMethMeta;
    private ItemStack ShroomItem;
    private ItemMeta ShroomMeta;
    private ItemStack SCItem;
    private ItemMeta SCMeta;
    private ItemStack Sbark;
    private ItemMeta SbarkMeta;
    private ItemStack MethylChloride;
    private ItemMeta MethylChlorideMeta;
    private ItemStack Mercury;
    private ItemMeta MercuryMeta;
    private ItemStack Exit;
    private ItemMeta ExitMeta;
    private ItemStack Safrole;
    private ItemMeta SafroleMeta;

    Items() {
        this.citem = new ItemStack(Material.SUGAR);
        this.citemmeta = this.citem.getItemMeta();
        this.citemmeta.setDisplayName(ChatColor.WHITE + "Cocaine");
        ArrayList<String> clore = new ArrayList();
        clore.add(ChatColor.WHITE + "Cocaine, also known as coke is a");
        clore.add(ChatColor.WHITE + "strong stimulant most frequently");
        clore.add(ChatColor.WHITE + "used as a recreational drug.");
        clore.add("" + ChatColor.WHITE);
        this.citemmeta.setLore(clore);
        this.citem.setItemMeta(this.citemmeta);
        this.clitem = new ItemStack(Material.KELP);
        this.clitemmeta = this.clitem.getItemMeta();
        this.clitemmeta.setDisplayName(ChatColor.WHITE + "Coca Leaf");
        ArrayList<String> cllore = new ArrayList();
        cllore.add(ChatColor.WHITE + "Used to create Cocaine!");
        this.clitemmeta.setLore(cllore);
        this.clitem.setItemMeta(this.clitemmeta);
        this.cpitem = new ItemStack(Material.TALL_GRASS);
        this.cpitemmeta = this.cpitem.getItemMeta();
        this.cpitemmeta.setDisplayName(ChatColor.WHITE + "Coca Plant");
        ArrayList<String> cplore = new ArrayList();
        cplore.add(ChatColor.WHITE + "Used to farm Coca Leaves!");
        this.cpitemmeta.setLore(cplore);
        this.cpitem.setItemMeta(this.cpitemmeta);
        this.hitem = new ItemStack(Material.GRAY_DYE);
        this.hitemmeta = this.hitem.getItemMeta();
        this.hitemmeta.setDisplayName(ChatColor.RED + "Heroin");
        ArrayList<String> hlore = new ArrayList();
        hlore.add(ChatColor.WHITE + "Heroin, also known as diamorphine among");
        hlore.add(ChatColor.WHITE + "other names, is an opiod most commonly");
        hlore.add(ChatColor.WHITE + "used as a recreational drug for ");
        hlore.add(ChatColor.WHITE + "its euphorice effects.");
        hlore.add("" + ChatColor.WHITE);
        this.hitemmeta.setLore(hlore);
        this.hitem.setItemMeta(this.hitemmeta);
        this.mitem = new ItemStack(Material.LIGHT_GRAY_DYE);
        this.mmmeta = this.mitem.getItemMeta();
        this.mmmeta.setDisplayName(ChatColor.RED + "Morphine");
        ArrayList<String> mlore = new ArrayList();
        mlore.add(ChatColor.WHITE + "Used to create Heroin!");
        this.mmmeta.setLore(mlore);
        this.mitem.setItemMeta(this.mmmeta);
        this.oitem = new ItemStack(Material.PINK_DYE);
        this.oitemmeta = this.oitem.getItemMeta();
        this.oitemmeta.setDisplayName(ChatColor.RED + "Opium");
        ArrayList<String> olore = new ArrayList();
        olore.add(ChatColor.WHITE + "Used to create Morphine!");
        this.oitemmeta.setLore(olore);
        this.oitem.setItemMeta(this.oitemmeta);
        this.opitem = new ItemStack(Material.ALLIUM);
        this.opitemmeta = this.opitem.getItemMeta();
        this.opitemmeta.setDisplayName(ChatColor.RED + "Opium Poppy");
        ArrayList<String> oplore = new ArrayList();
        oplore.add(ChatColor.WHITE + "Used to plant an Opium Poppy,");
        oplore.add(ChatColor.WHITE + "which you can harvest to get");
        oplore.add(ChatColor.WHITE + "Opium Poppy Seed Pods!");
        this.opitemmeta.setLore(oplore);
        this.opitem.setItemMeta(this.opitemmeta);
        this.opspitem = new ItemStack(Material.LIME_DYE);
        this.opspitemmeta = this.opspitem.getItemMeta();
        this.opspitemmeta.setDisplayName(ChatColor.RED + "Opium Poppy Seed Pods");
        ArrayList<String> opsplore = new ArrayList();
        opsplore.add(ChatColor.WHITE + "Used to obtain Opium!");
        this.opspitemmeta.setLore(opsplore);
        this.opspitem.setItemMeta(this.opspitemmeta);
        this.witem = new ItemStack(Material.GREEN_DYE);
        this.witemmeta = this.witem.getItemMeta();
        this.witemmeta.setDisplayName(ChatColor.DARK_GREEN + "Marijuana");
        ArrayList<String> wlore = new ArrayList();
        wlore.add(ChatColor.WHITE + "Marijuana is a psychoactive drug");
        wlore.add(ChatColor.WHITE + "from the Cannabis plant used for");
        wlore.add(ChatColor.WHITE + "medical and recreational purposes.");
        wlore.add("" + ChatColor.WHITE);
        this.witemmeta.setLore(wlore);
        this.witem.setItemMeta(this.witemmeta);
        this.HashItem = new ItemStack(Material.GREEN_DYE);
        this.HashMeta = this.HashItem.getItemMeta();
        this.HashMeta.setDisplayName(ChatColor.GOLD + "Hashish");
        ArrayList<String> HashLore = new ArrayList();
        HashLore.add(ChatColor.WHITE + "Hashish, or hash, is a");
        HashLore.add(ChatColor.WHITE + "drug made from the resin");
        HashLore.add(ChatColor.WHITE + "of the cannabis plant.");
        HashLore.add("" + ChatColor.WHITE);
        this.HashMeta.setLore(HashLore);
        this.HashItem.setItemMeta(this.HashMeta);
        this.wsitem = new ItemStack(Material.LARGE_FERN);
        this.wsitemmeta = this.wsitem.getItemMeta();
        this.wsitemmeta.setDisplayName(ChatColor.GREEN + "Marijuana Plant");
        ArrayList<String> wslore = new ArrayList();
        wslore.add(ChatColor.WHITE + "Use to harvest");
        wslore.add(ChatColor.WHITE + "marijuana!");
        this.wsitemmeta.setLore(wslore);
        this.wsitem.setItemMeta(this.wsitemmeta);
        this.bluntitem = new ItemStack(Material.STICK);
        this.bluntitemmeta = this.bluntitem.getItemMeta();
        this.bluntitemmeta.setDisplayName(ChatColor.DARK_GRAY + "<Not lit>");
        ArrayList<String> bluntlore = new ArrayList();
        bluntlore.add(ChatColor.WHITE + "Right click with flint and steel");
        bluntlore.add(ChatColor.WHITE + "to light the blunt!");
        this.bluntitemmeta.setLore(bluntlore);
        this.bluntitem.setItemMeta(this.bluntitemmeta);
        this.LitBlunt = new ItemStack(Material.TORCH);
        this.litbluntitem = this.bluntitem.getItemMeta();
        this.litbluntitem.setDisplayName(ChatColor.DARK_GRAY + "Blunt" + ChatColor.RED + " <Lit>");
        ArrayList<String> litbluntlore = new ArrayList();
        litbluntlore.add(ChatColor.WHITE + "Right Click to hit a blunt!");
        this.litbluntitem.setLore(litbluntlore);
        this.LitBlunt.setItemMeta(this.litbluntitem);
        this.methitem = new ItemStack(Material.CYAN_DYE);
        this.methmeta = this.methitem.getItemMeta();
        this.methmeta.setDisplayName(ChatColor.BLUE + "Methamphetamine");
        ArrayList<String> methlore = new ArrayList();
        methlore.add(ChatColor.WHITE + "Methamphetamine is a strong");
        methlore.add(ChatColor.WHITE + "and highly addictive drug that");
        methlore.add(ChatColor.WHITE + "affects the central nervous system");
        methlore.add("" + ChatColor.WHITE);
        this.methmeta.setLore(methlore);
        this.methitem.setItemMeta(this.methmeta);
        this.hclitem = new ItemStack(Material.WATER_BUCKET);
        this.hclmeta = this.hclitem.getItemMeta();
        this.hclmeta.setDisplayName(ChatColor.BLUE + "Hydrochloric acid");
        ArrayList<String> hcllore = new ArrayList();
        hcllore.add(ChatColor.WHITE + "Used for the production");
        hcllore.add(ChatColor.WHITE + "of Methamphetamine!");
        hcllore.add(ChatColor.WHITE + "- Can be thrown on other players");
        hcllore.add(ChatColor.WHITE + "and entities!");
        this.hclmeta.setLore(hcllore);
        this.hclitem.setItemMeta(this.hclmeta);
        this.pitem = new ItemStack(Material.RED_DYE);
        this.pmeta = this.pitem.getItemMeta();
        this.pmeta.setDisplayName(ChatColor.BLUE + "Phosphorus");
        ArrayList<String> plore = new ArrayList();
        plore.add(ChatColor.WHITE + "Used for the production");
        plore.add(ChatColor.WHITE + "of Methamphetamine!");
        this.pmeta.setLore(plore);
        this.pitem.setItemMeta(this.pmeta);
        this.iitem = new ItemStack(Material.PURPLE_DYE);
        this.imeta = this.iitem.getItemMeta();
        this.imeta.setDisplayName(ChatColor.BLUE + "Iodine");
        ArrayList<String> ilore = new ArrayList();
        ilore.add(ChatColor.WHITE + "Used for the production");
        ilore.add(ChatColor.WHITE + "of Methamphetamine!");
        this.imeta.setLore(ilore);
        this.iitem.setItemMeta(this.imeta);
        this.eitem = new ItemStack(Material.BLUE_DYE);
        this.emeta = this.eitem.getItemMeta();
        this.emeta.setDisplayName(ChatColor.BLUE + "Ephedrine");
        ArrayList<String> elore = new ArrayList();
        elore.add(ChatColor.WHITE + "Used for the production");
        elore.add(ChatColor.WHITE + "of Methamphetamine!");
        this.emeta.setLore(elore);
        this.eitem.setItemMeta(this.emeta);
        this.esitem = new ItemStack(Material.RED_TULIP);
        this.esmeta = this.esitem.getItemMeta();
        this.esmeta.setDisplayName(ChatColor.BLUE + "Ephedra");
        ArrayList<String> eslore = new ArrayList();
        eslore.add(ChatColor.WHITE + "Use to harvest");
        eslore.add(ChatColor.WHITE + "Ephedrine!");
        this.esmeta.setLore(eslore);
        this.esitem.setItemMeta(this.esmeta);
        this.Ecitem = new ItemStack(Material.IRON_NUGGET);
        this.Ecmeta = this.Ecitem.getItemMeta();
        this.Ecmeta.setDisplayName(ChatColor.GOLD + "Ecstacy");
        ArrayList<String> Eclore = new ArrayList();
        Eclore.add(ChatColor.WHITE + "Ecstasy or molly, is a psychoactive drug ");
        Eclore.add(ChatColor.WHITE + "primarily used as a recreational drug.");
        Eclore.add(ChatColor.WHITE + "The desired effects include altered sensations,");
        Eclore.add(ChatColor.WHITE + "increased energy, empathy, and pleasure.");
        Eclore.add(ChatColor.WHITE + "         ");
        this.Ecmeta.setLore(Eclore);
        this.Ecitem.setItemMeta(this.Ecmeta);
        this.ssitem = new ItemStack(Material.OAK_SAPLING);
        this.ssmeta = this.ssitem.getItemMeta();
        this.ssmeta.setDisplayName(ChatColor.GOLD + "Sassafras Sapling");
        ArrayList<String> sslore = new ArrayList();
        sslore.add(ChatColor.WHITE + "Used to harvest Safrole!");
        sslore.add(ChatColor.WHITE + "- the ingredient used to ");
        sslore.add(ChatColor.WHITE + "create Ecstacy!");
        this.ssmeta.setLore(sslore);
        this.ssitem.setItemMeta(this.ssmeta);
        this.Sbark = new ItemStack(Material.NETHER_BRICK);
        this.SbarkMeta = this.Sbark.getItemMeta();
        this.SbarkMeta.setDisplayName(ChatColor.GOLD + "Sassafras Bark");
        ArrayList<String> SbarkLore = new ArrayList();
        SbarkLore.add(ChatColor.WHITE + "Used to obtain Safrole!");
        SbarkLore.add(ChatColor.WHITE + "which is a key ingredient");
        SbarkLore.add(ChatColor.WHITE + "in the production of Ecstacy.");
        this.SbarkMeta.setLore(SbarkLore);
        this.Sbark.setItemMeta(this.SbarkMeta);
        this.Safrole = new ItemStack(Material.SUNFLOWER);
        this.SafroleMeta = this.Safrole.getItemMeta();
        this.SafroleMeta.setDisplayName(ChatColor.GOLD + "Safrole");
        ArrayList<String> SafroleLore = new ArrayList();
        SafroleLore.add(ChatColor.WHITE + "A key ingredient to the");
        SafroleLore.add(ChatColor.WHITE + "production of Ecstacy");
        this.SafroleMeta.setLore(SafroleLore);
        this.Safrole.setItemMeta(this.SafroleMeta);
        this.MethylChloride = new ItemStack(Material.GLASS_BOTTLE);
        this.MethylChlorideMeta = this.MethylChloride.getItemMeta();
        this.MethylChlorideMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        this.MethylChlorideMeta.setDisplayName(ChatColor.GOLD + "Methylamine Chloride");
        ArrayList<String> MethylChlorideLore = new ArrayList();
        MethylChlorideLore.add(ChatColor.WHITE + "This gas is a key ingredient");
        MethylChlorideLore.add(ChatColor.WHITE + "to the production of Ecstacy");
        this.MethylChlorideMeta.setLore(MethylChlorideLore);
        this.MethylChloride.setItemMeta(this.MethylChlorideMeta);
        this.Mercury = new ItemStack(Material.GUNPOWDER);
        this.MercuryMeta = this.Mercury.getItemMeta();
        this.MercuryMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        this.MercuryMeta.setDisplayName(ChatColor.GOLD + "Mercury");
        ArrayList<String> MercuryLore = new ArrayList();
        MercuryLore.add(ChatColor.WHITE + "Highly toxic element, which is");
        MercuryLore.add(ChatColor.WHITE + "used to create Ecstacy");
        this.MercuryMeta.setLore(MercuryLore);
        this.Mercury.setItemMeta(this.MercuryMeta);
        this.SyrItem = new ItemStack(Material.GOLDEN_HOE);
        this.SyrMeta = this.SyrItem.getItemMeta();
        this.SyrMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Syringe" + ChatColor.DARK_RED + "<" + ChatColor.DARK_GRAY + "EMPTY" + ChatColor.DARK_RED + ">");
        ArrayList<String> SyrLore = new ArrayList();
        SyrLore.add(ChatColor.WHITE + "Drugs can be inserted into");
        SyrLore.add(ChatColor.WHITE + "a syringe to boost their effects!");
        SyrLore.add("" + ChatColor.WHITE);
        SyrLore.add(ChatColor.WHITE + "To insert a drug: Have the drug");
        SyrLore.add(ChatColor.WHITE + "on your cursor, and hover over an empty");
        SyrLore.add(ChatColor.WHITE + "syringe, then left click to put it in!");
        SyrLore.add("" + ChatColor.WHITE);
        SyrLore.add(ChatColor.WHITE + "To remove a drug: Simply right click");
        SyrLore.add(ChatColor.WHITE + "the filled syringe to get the drug back!");
        this.SyrMeta.setLore(SyrLore);
        this.SyrItem.setItemMeta(this.SyrMeta);
        this.SyrCocaineItem = new ItemStack(Material.GOLDEN_HOE);
        this.SyrCocaineMeta = this.SyrCocaineItem.getItemMeta();
        this.SyrCocaineMeta.setDisplayName(ChatColor.WHITE + "Syringe" + ChatColor.DARK_RED + "<" + ChatColor.RED + "COCAINE" + ChatColor.DARK_RED + ">");
        ArrayList<String> SyrCocaineLore = new ArrayList();
        SyrCocaineLore.add(ChatColor.WHITE + "Right click to inject Cocaine!");
        SyrCocaineLore.add("" + ChatColor.WHITE);
        SyrCocaineLore.add(ChatColor.WHITE + "Left click to remove ");
        SyrCocaineLore.add(ChatColor.WHITE + "Cocaine from Syringe!");
        this.SyrCocaineMeta.setLore(SyrCocaineLore);
        this.SyrCocaineItem.setItemMeta(this.SyrCocaineMeta);
        this.SyrHeroinItem = new ItemStack(Material.GOLDEN_HOE);
        this.SyrHeroinMeta = this.SyrItem.getItemMeta();
        this.SyrHeroinMeta.setDisplayName(ChatColor.WHITE + "Syringe" + ChatColor.DARK_RED + "<" + ChatColor.RED + "HEROIN" + ChatColor.DARK_RED + ">");
        ArrayList<String> SyrHeroinLore = new ArrayList();
        SyrHeroinLore.add(ChatColor.WHITE + "Right click to inject Heroin!");
        SyrHeroinLore.add("" + ChatColor.WHITE);
        SyrHeroinLore.add(ChatColor.WHITE + "Left click to remove ");
        SyrHeroinLore.add(ChatColor.WHITE + "Heroin from Syringe!");
        this.SyrHeroinMeta.setLore(SyrHeroinLore);
        this.SyrHeroinItem.setItemMeta(this.SyrHeroinMeta);
        this.SyrMethItem = new ItemStack(Material.GOLDEN_HOE);
        this.SyrMethMeta = this.SyrItem.getItemMeta();
        ArrayList<String> SyrMethLore = new ArrayList();
        this.SyrMethMeta.setDisplayName("" + ChatColor.WHITE + ChatColor.UNDERLINE + "Syringe" + ChatColor.DARK_RED + "<" + ChatColor.RED + "METH" + ChatColor.DARK_RED + ">");
        SyrMethLore.add(ChatColor.WHITE + "Right click to inject Meth!");
        SyrMethLore.add("" + ChatColor.WHITE);
        SyrMethLore.add(ChatColor.WHITE + "Left click to remove ");
        SyrMethLore.add(ChatColor.WHITE + "Meth from Syringe!");
        this.SyrMethMeta.setLore(SyrMethLore);
        this.SyrMethItem.setItemMeta(this.SyrMethMeta);
        this.ShroomItem = new ItemStack(Material.BROWN_MUSHROOM);
        this.ShroomMeta = this.ShroomItem.getItemMeta();
        this.ShroomMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Psilocybin Mushroom");
        ArrayList<String> ShroomLore = new ArrayList();
        ShroomLore.add(ChatColor.WHITE + "Psilocybin mushrooms are wild ");
        ShroomLore.add(ChatColor.WHITE + "or cultivated mushrooms that contain");
        ShroomLore.add(ChatColor.WHITE + "psilocybin, a naturally-occurring");
        ShroomLore.add(ChatColor.WHITE + "psychoactive and hallucinogenic compound.");
        ShroomLore.add("" + ChatColor.WHITE);
        this.ShroomMeta.setLore(ShroomLore);
        this.ShroomItem.setItemMeta(this.ShroomMeta);
        this.SCItem = new ItemStack(Material.PLAYER_HEAD);
        this.SCMeta = (SkullMeta)this.SCItem.getItemMeta();
        this.SCMeta.setDisplayName("" + ChatColor.LIGHT_PURPLE + ChatColor.BOLD + ChatColor.UNDERLINE + "Psilocybin Mushroom Chamber");
        ArrayList<String> SClore = new ArrayList();
        SClore.add(ChatColor.WHITE + "Use to farm Psilocybin mushrooms!");
        this.Exit = new ItemStack(Material.BARRIER);
        this.ExitMeta = this.Exit.getItemMeta();
        this.ExitMeta.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Click to exit!");
        this.Exit.setItemMeta(this.ExitMeta);
        this.SCMeta.setLore(SClore);
        this.SCItem.setItemMeta(this.SCMeta);
        this.setCitem(this.citem);
        this.setCitemmeta(this.citemmeta);
        this.setClitem(this.clitem);
        this.setClitemmeta(this.clitemmeta);
        this.setCpitem(this.cpitem);
        this.setCpitemmeta(this.cpitemmeta);
        this.setHitem(this.hitem);
        this.setHitemmeta(this.hitemmeta);
        this.setMitem(this.mitem);
        this.setMitemmeta(this.mmmeta);
        this.setOitem(this.oitem);
        this.setOitemmeta(this.oitemmeta);
        this.setOpitem(this.opitem);
        this.setOpitemmeta(this.opitemmeta);
        this.setOpspitem(this.opspitem);
        this.setOpspitemmeta(this.opspitemmeta);
        this.setWitem(this.witem);
        this.setWitemmeta(this.witemmeta);
        this.setHashItem(this.HashItem);
        this.setHashMeta(this.HashMeta);
        this.setWsitem(this.wsitem);
        this.setWsitemmeta(this.wsitemmeta);
        this.setBluntitem(this.bluntitem);
        this.setBluntitemmeta(this.bluntitemmeta);
        this.setLitBlunt(this.LitBlunt);
        this.setLitbluntmeta(this.litbluntitem);
        this.setMethitem(this.methitem);
        this.setMethmeta(this.methmeta);
        this.setHclitem(this.hclitem);
        this.setHclmeta(this.hclmeta);
        this.setPitem(this.pitem);
        this.setPmeta(this.pmeta);
        this.setIitem(this.iitem);
        this.setImeta(this.imeta);
        this.setEitem(this.eitem);
        this.setEmeta(this.emeta);
        this.setEsitem(this.esitem);
        this.setEsmeta(this.esmeta);
        this.setSsitem(this.ssitem);
        this.setSsmeta(this.ssmeta);
        this.setSyrItem(this.SyrItem);
        this.setSyrMeta(this.SyrMeta);
        this.setSyrCocainemeta(this.SyrCocaineMeta);
        this.setSyrCocaineItem(this.SyrCocaineItem);
        this.setSyrHeroinmeta(this.SyrHeroinMeta);
        this.setSyrHeroinItem(this.SyrHeroinItem);
        this.setSyrMethMeta(this.SyrMethMeta);
        this.setSyrMethItem(this.SyrMethItem);
        this.setShroomItem(this.ShroomItem);
        this.setShroomMeta(this.ShroomMeta);
        this.setSbark(this.Sbark);
        this.setSbarkMeta(this.SbarkMeta);
        this.setSafrole(this.Safrole);
        this.setSafroleMeta(this.SafroleMeta);
        this.setMethylChloride(this.MethylChloride);
        this.setMethylChlorideMeta(this.MethylChlorideMeta);
        this.setMercury(this.Mercury);
        this.setMercuryMeta(this.MercuryMeta);
        this.setExititem(this.Exit);
    }

    public void setCitem(ItemStack item) {
        this.citem = item;
    }

    public ItemStack getCitem(Integer amt) {
        this.citem.setAmount(amt);
        return this.citem;
    }

    public void setCitemmeta(ItemMeta meta) {
        this.citemmeta = meta;
    }

    public ItemMeta getCitemmeta() {
        return this.citemmeta;
    }

    public void setClitem(ItemStack item) {
        this.clitem = item;
    }

    public ItemStack getClitem(Integer amt) {
        this.clitem.setAmount(amt);
        return this.clitem;
    }

    public void setClitemmeta(ItemMeta meta) {
        this.clitemmeta = meta;
    }

    public ItemMeta getClitemmeta() {
        return this.clitemmeta;
    }

    public void setCpitem(ItemStack item) {
        this.cpitem = item;
    }

    public ItemStack getCpitem(Integer amt) {
        this.cpitem.setAmount(amt);
        return this.cpitem;
    }

    public void setCpitemmeta(ItemMeta meta) {
        this.cpitemmeta = meta;
    }

    public ItemMeta getCpitemmeta() {
        return this.cpitemmeta;
    }

    public void setHitem(ItemStack item) {
        this.hitem = item;
    }

    public ItemStack getHitem(Integer amt) {
        this.hitem.setAmount(amt);
        return this.hitem;
    }

    public void setHitemmeta(ItemMeta meta) {
        this.hitemmeta = meta;
    }

    public ItemMeta getHitemmeta() {
        return this.hitemmeta;
    }

    public void setMitem(ItemStack item) {
        this.mitem = item;
    }

    public ItemStack getMitem(Integer amt) {
        this.mitem.setAmount(amt);
        return this.mitem;
    }

    public void setMitemmeta(ItemMeta meta) {
        this.mmmeta = meta;
    }

    public ItemMeta getMitemmeta() {
        return this.mmmeta;
    }

    public void setOitem(ItemStack item) {
        this.oitem = item;
    }

    public ItemStack getOitem(Integer amt) {
        this.oitem.setAmount(amt);
        return this.oitem;
    }

    public void setOitemmeta(ItemMeta meta) {
        this.oitemmeta = meta;
    }

    public ItemMeta getOitemmeta() {
        return this.oitemmeta;
    }

    public void setOpitem(ItemStack item) {
        this.opitem = item;
    }

    public ItemStack getOpitem(Integer amt) {
        this.opitem.setAmount(amt);
        return this.opitem;
    }

    public void setOpitemmeta(ItemMeta meta) {
        this.opitemmeta = meta;
    }

    public ItemMeta getOpitemmeta() {
        return this.opitemmeta;
    }

    public void setOpspitem(ItemStack item) {
        this.opspitem = item;
    }

    public ItemStack getOpspitem(Integer amt) {
        this.opspitem.setAmount(amt);
        return this.opspitem;
    }

    public void setOpspitemmeta(ItemMeta meta) {
        this.opspitemmeta = meta;
    }

    public ItemMeta getOpspitemmeta() {
        return this.opspitemmeta;
    }

    public void setEcitem(ItemStack item) {
        this.Ecitem = item;
    }

    public ItemStack getEcitem(Integer amt) {
        this.Ecitem.setAmount(amt);
        return this.Ecitem;
    }

    public void setEcmeta(ItemMeta meta) {
        this.Ecmeta = meta;
    }

    public ItemMeta getEcmeta() {
        return this.Ecmeta;
    }

    public void setWitem(ItemStack item) {
        this.witem = item;
    }

    public ItemStack getWitem(Integer amt) {
        this.witem.setAmount(amt);
        return this.witem;
    }

    public void setHashItem(ItemStack item) {
        this.HashItem = item;
    }

    public ItemStack getHashItem(Integer amt) {
        this.HashItem.setAmount(amt);
        return this.HashItem;
    }

    public void setHashMeta(ItemMeta meta) {
        this.HashMeta = meta;
    }

    public ItemMeta getHashMeta() {
        return this.HashMeta;
    }

    public void setWitemmeta(ItemMeta meta) {
        this.witemmeta = meta;
    }

    public ItemMeta getWitemmeta() {
        return this.witemmeta;
    }

    public void setWsitem(ItemStack item) {
        this.wsitem = item;
    }

    public ItemStack getWsitem(Integer amt) {
        this.wsitem.setAmount(amt);
        return this.wsitem;
    }

    public void setWsitemmeta(ItemMeta meta) {
        this.wsitemmeta = meta;
    }

    public ItemMeta getWsitemmeta() {
        return this.wsitemmeta;
    }

    public void setBluntitem(ItemStack item) {
        this.bluntitem = item;
    }

    public ItemStack getBluntitem(Integer amt) {
        this.bluntitem.setAmount(amt);
        return this.bluntitem;
    }

    public void setBluntitemmeta(ItemMeta meta) {
        this.bluntitemmeta = meta;
    }

    public ItemMeta getBluntitemmeta() {
        return this.bluntitemmeta;
    }

    public void setLitBlunt(ItemStack item) {
        this.LitBlunt = item;
    }

    public ItemStack getLitBlunt(Integer amt) {
        this.LitBlunt.setAmount(amt);
        return this.LitBlunt;
    }

    public void setLitbluntmeta(ItemMeta meta) {
        this.litbluntitem = meta;
    }

    public ItemMeta getLitbluntmeta() {
        return this.litbluntitem;
    }

    public void setMethitem(ItemStack item) {
        this.methitem = item;
    }

    public ItemStack getMethitem(Integer amt) {
        this.methitem.setAmount(amt);
        return this.methitem;
    }

    public void setMethmeta(ItemMeta meta) {
        this.methmeta = meta;
    }

    public ItemMeta getMethmeta() {
        return this.methmeta;
    }

    public void setHclitem(ItemStack item) {
        this.hclitem = item;
    }

    public ItemStack getHclitem(Integer amt) {
        this.hclitem.setAmount(amt);
        return this.hclitem;
    }

    public void setHclmeta(ItemMeta meta) {
        this.hclmeta = meta;
    }

    public ItemMeta getHclmeta() {
        return this.hclmeta;
    }

    public void setPitem(ItemStack item) {
        this.pitem = item;
    }

    public ItemStack getPitem(Integer amt) {
        this.pitem.setAmount(amt);
        return this.pitem;
    }

    public void setPmeta(ItemMeta meta) {
        this.pmeta = meta;
    }

    public ItemMeta getPmeta() {
        return this.pmeta;
    }

    public void setIitem(ItemStack item) {
        this.iitem = item;
    }

    public ItemStack getIitem(Integer amt) {
        this.iitem.setAmount(amt);
        return this.iitem;
    }

    public void setImeta(ItemMeta meta) {
        this.imeta = meta;
    }

    public ItemMeta getImeta() {
        return this.imeta;
    }

    public void setEitem(ItemStack item) {
        this.eitem = item;
    }

    public ItemStack getEitem(Integer amt) {
        this.eitem.setAmount(amt);
        return this.eitem;
    }

    public void setEmeta(ItemMeta meta) {
        this.emeta = meta;
    }

    public ItemMeta getEmeta() {
        return this.emeta;
    }

    public void setEsitem(ItemStack item) {
        this.esitem = item;
    }

    public ItemStack getEsitem(Integer amt) {
        this.esitem.setAmount(amt);
        return this.esitem;
    }

    public void setEsmeta(ItemMeta meta) {
        this.esmeta = meta;
    }

    public ItemMeta getEsmeta() {
        return this.esmeta;
    }

    public void setSsitem(ItemStack item) {
        this.ssitem = item;
    }

    public ItemStack getSsitem(Integer amt) {
        this.ssitem.setAmount(amt);
        return this.ssitem;
    }

    public void setSsmeta(ItemMeta meta) {
        this.ssmeta = meta;
    }

    public ItemMeta getSsmeta() {
        return this.ssmeta;
    }

    public void setSyrItem(ItemStack item) {
        this.SyrItem = item;
    }

    public ItemStack getSyrItem(Integer amt) {
        this.SyrItem.setAmount(amt);
        return this.SyrItem;
    }

    public void setSyrMeta(ItemMeta meta) {
        this.SyrMeta = meta;
    }

    public ItemMeta getSyrMeta() {
        return this.SyrMeta;
    }

    public void setSyrCocainemeta(ItemMeta meta) {
        this.SyrCocaineMeta = meta;
    }

    public ItemMeta getSyrCocaineMeta() {
        return this.SyrCocaineMeta;
    }

    public void setSyrCocaineItem(ItemStack item) {
        this.SyrCocaineItem = item;
    }

    public ItemStack getSyrCocaineItem(Integer amt) {
        this.SyrCocaineItem.setAmount(amt);
        return this.SyrCocaineItem;
    }

    public void setSyrHeroinmeta(ItemMeta meta) {
        this.SyrHeroinMeta = meta;
    }

    public ItemMeta getSyrHeroinMeta() {
        return this.SyrHeroinMeta;
    }

    public void setSyrHeroinItem(ItemStack item) {
        this.SyrHeroinItem = item;
    }

    public ItemStack getSyrHeroinItem(Integer amt) {
        this.SyrHeroinItem.setAmount(amt);
        return this.SyrHeroinItem;
    }

    public void setSyrMethMeta(ItemMeta meta) {
        this.SyrMethMeta = meta;
    }

    public ItemMeta getSyrMethMeta() {
        return this.SyrMethMeta;
    }

    public void setSyrMethItem(ItemStack item) {
        this.SyrMethItem = item;
    }

    public ItemStack getSyrMethItem(Integer amt) {
        this.SyrMethItem.setAmount(amt);
        return this.SyrMethItem;
    }

    public void setShroomItem(ItemStack item) {
        this.ShroomItem = item;
    }

    public ItemStack getShroomItem(Integer amt) {
        this.ShroomItem.setAmount(amt);
        return this.ShroomItem;
    }

    public void setShroomMeta(ItemMeta meta) {
        this.ShroomMeta = meta;
    }

    public ItemMeta getShroomMeta() {
        return this.ShroomMeta;
    }

    public void setSbark(ItemStack item) {
        this.Sbark = item;
    }

    public ItemStack getSbark(Integer amt) {
        this.Sbark.setAmount(amt);
        return this.Sbark;
    }

    public void setSbarkMeta(ItemMeta meta) {
        this.SbarkMeta = meta;
    }

    public ItemMeta getSbarkMeta() {
        return this.SbarkMeta;
    }

    public void setSafrole(ItemStack item) {
        this.Safrole = item;
    }

    public ItemStack getSafrole(Integer amt) {
        this.Safrole.setAmount(amt);
        return this.Safrole;
    }

    public void setSafroleMeta(ItemMeta meta) {
        this.SafroleMeta = meta;
    }

    public ItemMeta getSafroleMeta() {
        return this.SafroleMeta;
    }

    public void setMethylChloride(ItemStack item) {
        this.MethylChloride = item;
    }

    public ItemStack getMethylChloride(Integer amt) {
        this.MethylChloride.setAmount(amt);
        return this.MethylChloride;
    }

    public void setMethylChlorideMeta(ItemMeta meta) {
        this.MethylChlorideMeta = meta;
    }

    public ItemMeta getMethylChlorideMeta() {
        return this.MethylChlorideMeta;
    }

    public void setMercury(ItemStack item) {
        this.Mercury = item;
    }

    public ItemStack getMercury(Integer amt) {
        this.Mercury.setAmount(amt);
        return this.Mercury;
    }

    public void setMercuryMeta(ItemMeta meta) {
        this.MercuryMeta = meta;
    }

    public ItemMeta getMercuryMeta() {
        return this.MercuryMeta;
    }

    public void setExititem(ItemStack itemstack) {
        this.Exit = itemstack;
    }

    public ItemStack getExititem() {
        return this.Exit;
    }
}