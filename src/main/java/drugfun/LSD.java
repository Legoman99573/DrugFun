package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class LSD implements Listener {
    private Main main;
    private ItemStack MorningGloryPlant;
    private ItemMeta MorningGloryPlantMeta;
    private ItemStack MorningGloryPlantSeeds;
    private ItemMeta MorningGloryPlantSeedsMeta;
    private ItemStack LysergicAcid;
    private ItemMeta LysergicAcidMeta;
    private ItemStack Chloroform;
    private ItemMeta ChloroformMeta;
    private ItemStack Ethanol;
    private ItemMeta EthanolMeta;
    private ItemStack Acid;
    private ItemMeta AcidMeta;
    Material m;
    public ArrayList<Player> HCooldown = new ArrayList();
    Random r = new Random();
    ArrayList<Player> knockout = new ArrayList();

    LSD(Main main) {
        this.main = main;
        this.MorningGloryPlant = new ItemStack(Material.CORNFLOWER);
        this.MorningGloryPlantMeta = this.MorningGloryPlant.getItemMeta();
        assert this.MorningGloryPlantMeta != null;
        this.MorningGloryPlantMeta.setDisplayName(ChatColor.AQUA + "Morning Glory Plant");
        ArrayList<String> MorningGloryLore = new ArrayList();
        MorningGloryLore.add(ChatColor.WHITE + "Used to harvest Morning Glory seeds");
        this.MorningGloryPlantMeta.setLore(MorningGloryLore);
        this.MorningGloryPlant.setItemMeta(this.MorningGloryPlantMeta);
        this.MorningGloryPlantSeeds = new ItemStack(Material.MELON_SEEDS);
        this.MorningGloryPlantSeedsMeta = this.MorningGloryPlantSeeds.getItemMeta();
        assert this.MorningGloryPlantSeedsMeta != null;
        this.MorningGloryPlantSeedsMeta.setDisplayName(ChatColor.AQUA + "Morning Glory Plant Seeds");
        ArrayList<String> MorningGlorySeedsLore = new ArrayList();
        MorningGlorySeedsLore.add(ChatColor.WHITE + "Used to extract Lysergic acid!");
        this.MorningGloryPlantSeedsMeta.setLore(MorningGlorySeedsLore);
        this.MorningGloryPlantSeeds.setItemMeta(this.MorningGloryPlantSeedsMeta);
        this.LysergicAcid = new ItemStack(Material.WATER_BUCKET);
        this.LysergicAcidMeta = this.LysergicAcid.getItemMeta();
        assert this.LysergicAcidMeta != null;
        this.LysergicAcidMeta.setDisplayName(ChatColor.AQUA + "Lysergic Acid");
        ArrayList<String> LysergicAcidLore = new ArrayList();
        LysergicAcidLore.add(ChatColor.WHITE + "Key ingredient in the production of LSD!");
        this.LysergicAcidMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        this.LysergicAcidMeta.setLore(LysergicAcidLore);
        this.LysergicAcid.setItemMeta(this.LysergicAcidMeta);
        this.Chloroform = new ItemStack(Material.PAPER);
        this.ChloroformMeta = this.Chloroform.getItemMeta();
        assert this.ChloroformMeta != null;
        this.ChloroformMeta.setDisplayName(ChatColor.AQUA + "Chloroform");
        ArrayList<String> ChloroformLore = new ArrayList();
        ChloroformLore.add(ChatColor.WHITE + "Key ingredient in the production of LSD!");
        ChloroformLore.add(ChatColor.WHITE + "can also be used to knockout other Players!");
        this.ChloroformMeta.setLore(ChloroformLore);
        this.Chloroform.setItemMeta(this.ChloroformMeta);
        this.Ethanol = new ItemStack(Material.WATER_BUCKET);
        this.EthanolMeta = this.Ethanol.getItemMeta();
        assert this.EthanolMeta != null;
        this.EthanolMeta.setDisplayName(ChatColor.AQUA + "Ethanol");
        ArrayList<String> EthanolLore = new ArrayList();
        EthanolLore.add(ChatColor.WHITE + "Key ingredient in the production of LSD!");
        this.EthanolMeta.setLore(EthanolLore);
        this.Ethanol.setItemMeta(this.EthanolMeta);
        this.Acid = new ItemStack(Material.PAPER);
        this.AcidMeta = this.Acid.getItemMeta();
        assert this.AcidMeta != null;
        this.AcidMeta.setDisplayName(ChatColor.AQUA + "LSD");
        ArrayList<String> AcidLore = new ArrayList();
        AcidLore.add(ChatColor.WHITE + "Lysergic acid diethylamide, also known");
        AcidLore.add(ChatColor.WHITE + "as acid,is a hallucinogenic drug.");
        AcidLore.add(ChatColor.WHITE + "Effects typically include altered");
        AcidLore.add(ChatColor.WHITE + "thoughts, feelings, and");
        AcidLore.add(ChatColor.WHITE + "awareness of one's surroundings.");
        AcidLore.add(ChatColor.WHITE + "Many users see or hear things");
        AcidLore.add(ChatColor.WHITE + "that do not exist");
        AcidLore.add("");
        this.AcidMeta.addEnchant(Enchantment.DURABILITY, 1, true);
        this.AcidMeta.setLore(AcidLore);
        this.Acid.setItemMeta(this.AcidMeta);
        this.setMorningGloryPlant(this.MorningGloryPlant);
        this.setMorningGloryPlantMeta(this.MorningGloryPlantMeta);
        this.setMorningGloryPlantSeeds(this.MorningGloryPlantSeeds);
        this.setMorningGloryPlantSeedsMeta(this.MorningGloryPlantSeedsMeta);
        this.setLysergicAcid(this.LysergicAcid);
        this.setLysergicAcidMeta(this.LysergicAcidMeta);
        this.setChloroform(this.Chloroform);
        this.setChloroformMeta(this.ChloroformMeta);
        this.setEthanol(this.Ethanol);
        this.setEthanolMeta(this.EthanolMeta);
        this.setAcid(this.Acid);
        this.setAcidMeta(this.AcidMeta);
    }

    public void onStampDesign(Player p) {
        Inventory i = Bukkit.createInventory((InventoryHolder)null, 54, HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("StampDesignTitle")), main));
        ItemStack stampButton = new ItemStack(Material.WRITABLE_BOOK);
        ItemMeta stampButtonMeta = stampButton.getItemMeta();
        assert stampButtonMeta != null;
        stampButtonMeta.setDisplayName(HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("StampDesignButton")), main));
        stampButton.setItemMeta(stampButtonMeta);
        i.setItem(45, new ItemStack(Material.WHITE_WOOL));
        i.setItem(46, new ItemStack(Material.ORANGE_WOOL));
        i.setItem(47, new ItemStack(Material.MAGENTA_WOOL));
        i.setItem(48, new ItemStack(Material.RED_WOOL));
        i.setItem(49, new ItemStack(Material.YELLOW_WOOL));
        i.setItem(50, new ItemStack(Material.GREEN_WOOL));
        i.setItem(51, new ItemStack(Material.LIGHT_BLUE_WOOL));
        i.setItem(52, new ItemStack(Material.GRAY_WOOL));
        i.setItem(44, new ItemStack(Material.BLACK_WOOL));
        i.setItem(35, new ItemStack(Material.CYAN_WOOL));
        i.setItem(26, new ItemStack(Material.LIME_WOOL));
        i.setItem(17, new ItemStack(Material.PURPLE_WOOL));
        i.setItem(8, new ItemStack(Material.BLUE_WOOL));
        i.setItem(53, stampButton);
        p.closeInventory();
        p.openInventory(i);
    }

    @EventHandler
    public void EcstacyStampInteractions(InventoryClickEvent e) {
        if (e.getView().getTitle().equals(HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("StampDesignTitle")), main))) {
            ArrayList<Integer> bannedSlots = new ArrayList();
            bannedSlots.add(8);
            bannedSlots.add(17);
            bannedSlots.add(26);
            bannedSlots.add(35);
            bannedSlots.add(44);
            bannedSlots.add(45);
            bannedSlots.add(46);
            bannedSlots.add(47);
            bannedSlots.add(48);
            bannedSlots.add(49);
            bannedSlots.add(50);
            bannedSlots.add(51);
            bannedSlots.add(52);
            bannedSlots.add(53);
            e.setCancelled(true);
            if (e.getRawSlot() == 53) {
                ArrayList<String> stamp = new ArrayList();
                StringBuilder l1 = new StringBuilder();
                StringBuilder l2 = new StringBuilder();
                StringBuilder l3 = new StringBuilder();
                StringBuilder l4 = new StringBuilder();
                StringBuilder l5 = new StringBuilder();
                int i = 0;

                while(true) {
                    if (i >= 44) {
                        stamp.add(l1.toString());
                        stamp.add(l2.toString());
                        stamp.add(l3.toString());
                        stamp.add(l4.toString());
                        stamp.add(l5.toString());
                        Player p = (Player)e.getWhoClicked();
                        ItemMeta meta = p.getItemInHand().getItemMeta();
                        ArrayList<String> lore = (ArrayList)meta.getLore();
                        assert lore != null;
                        if (lore.size() != 5 && lore.size() != 8) {
                            p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("StampPillError")), main));
                            break;
                        }

                        lore.add(l1.toString());
                        lore.add(l2.toString());
                        lore.add(l3.toString());
                        lore.add(l4.toString());
                        lore.add(l5.toString());
                        meta.setLore(lore);
                        p.getItemInHand().setItemMeta(meta);
                        p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("StampPill")), main));
                        p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
                        return;
                    }

                    if (i <= 8) {
                        if (Objects.requireNonNull(e.getClickedInventory()).getItem(i) != null) {
                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.WHITE_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.WHITE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.GOLD + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.LIGHT_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.RED_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.RED + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.YELLOW_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.YELLOW + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GREEN_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.DARK_GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GRAY_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.DARK_GRAY + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLACK_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.BLACK + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.CYAN_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.DARK_AQUA + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIME_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.PURPLE_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.DARK_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLUE_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.DARK_BLUE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE))) {
                                l1.append(ChatColor.BLUE + "▇");
                            }
                        } else {
                            l1.append(ChatColor.GRAY + "▇");
                        }
                    } else if (i > 8 && i <= 17) {
                        if (e.getClickedInventory().getItem(i) != null) {
                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.WHITE_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.WHITE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.GOLD + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.LIGHT_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.RED_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.RED + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.YELLOW_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.YELLOW + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GREEN_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.DARK_GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GRAY_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.DARK_GRAY + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLACK_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.BLACK + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.CYAN_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.DARK_AQUA + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIME_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.PURPLE_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.DARK_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLUE_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.DARK_BLUE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE))) {
                                l2.append(ChatColor.BLUE + "▇");
                            }
                        } else {
                            l2.append(ChatColor.GRAY + "▇");
                        }
                    } else if (i > 17 && i <= 26) {
                        if (e.getClickedInventory().getItem(i) != null) {
                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.WHITE_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.WHITE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.GOLD + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.LIGHT_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.RED_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.RED + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.YELLOW_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.YELLOW + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GREEN_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.DARK_GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GRAY_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.DARK_GRAY + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLACK_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.BLACK + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.CYAN_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.DARK_AQUA + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIME_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.PURPLE_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.DARK_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLUE_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.DARK_BLUE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE))) {
                                l3.append(ChatColor.BLUE + "▇");
                            }
                        } else {
                            l3.append(ChatColor.GRAY + "▇");
                        }
                    } else if (i > 26 && i <= 35) {
                        if (e.getClickedInventory().getItem(i) != null) {
                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.WHITE_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.WHITE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.GOLD + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.LIGHT_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.RED_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.RED + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.YELLOW_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.YELLOW + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GREEN_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.DARK_GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GRAY_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.DARK_GRAY + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLACK_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.BLACK + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.CYAN_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.DARK_AQUA + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIME_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.PURPLE_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.DARK_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLUE_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.DARK_BLUE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE))) {
                                l4.append(ChatColor.BLUE + "▇");
                            }
                        } else {
                            l4.append(ChatColor.GRAY + "▇");
                        }
                    } else if (i > 35 && i <= 44) {
                        if (e.getClickedInventory().getItem(i) != null) {
                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.WHITE_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.WHITE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.ORANGE_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.GOLD + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.MAGENTA_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.LIGHT_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.RED_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.RED + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.YELLOW_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.YELLOW + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GREEN_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.DARK_GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.GRAY_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.DARK_GRAY + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLACK_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.BLACK + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.CYAN_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.DARK_AQUA + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIME_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.GREEN + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.PURPLE_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.DARK_PURPLE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.BLUE_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.DARK_BLUE + "▇");
                            }

                            if (Objects.requireNonNull(e.getClickedInventory().getItem(i)).isSimilar(new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE))) {
                                l5.append(ChatColor.BLUE + "▇");
                            }
                        } else {
                            l5.append(ChatColor.GRAY + "▇");
                        }
                    }

                    ++i;
                }
            }

            if (e.isLeftClick()) {
                switch(e.getRawSlot()) {
                    case 8:
                        this.m = Material.BLUE_STAINED_GLASS_PANE;
                        break;
                    case 17:
                        this.m = Material.PURPLE_STAINED_GLASS_PANE;
                        break;
                    case 26:
                        this.m = Material.LIME_STAINED_GLASS_PANE;
                        break;
                    case 35:
                        this.m = Material.CYAN_STAINED_GLASS_PANE;
                        break;
                    case 44:
                        this.m = Material.BLACK_STAINED_GLASS_PANE;
                        break;
                    case 45:
                        this.m = Material.WHITE_STAINED_GLASS_PANE;
                        break;
                    case 46:
                        this.m = Material.ORANGE_STAINED_GLASS_PANE;
                        break;
                    case 47:
                        this.m = Material.MAGENTA_STAINED_GLASS_PANE;
                        break;
                    case 48:
                        this.m = Material.RED_STAINED_GLASS_PANE;
                        break;
                    case 49:
                        this.m = Material.YELLOW_STAINED_GLASS_PANE;
                        break;
                    case 50:
                        this.m = Material.GREEN_STAINED_GLASS_PANE;
                        break;
                    case 51:
                        this.m = Material.LIGHT_BLUE_STAINED_GLASS_PANE;
                        break;
                    case 52:
                        this.m = Material.GRAY_STAINED_GLASS_PANE;
                    case 53:
                        break;
                    default:
                        if (this.m != null) {
                            Objects.requireNonNull(e.getClickedInventory()).setItem(e.getRawSlot(), new ItemStack(this.m));
                        } else {
                            Objects.requireNonNull(e.getClickedInventory()).setItem(e.getRawSlot(), new ItemStack(Material.WHITE_STAINED_GLASS_PANE));
                        }
                }
            }

            if (e.isRightClick() && !bannedSlots.contains(e.getRawSlot()) && e.getRawSlot() < 53 && e.getCurrentItem() != null) {
                e.getCurrentItem().setAmount(0);
            }
        }

    }

    @EventHandler
    public void stamponEcstacy(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getItemInHand() != null && p.getItemInHand().hasItemMeta() && Objects.requireNonNull(p.getItemInHand().getItemMeta()).hasLore()) {
            if (!HexChat.translateHexCodes( (String)p.getItemInHand().getItemMeta().getLore().get(0), main).equals(ChatColor.WHITE + "Ecstasy or molly, is a psychoactive drug ") && !HexChat.translateHexCodes( (String)p.getItemInHand().getItemMeta().getLore().get(0), main).equals(ChatColor.WHITE + "Lysergic acid diethylamide, also known")) {
                return;
            }

            if (e.getHand() == EquipmentSlot.HAND && p.isSneaking() && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) && (Objects.requireNonNull(e.getClickedBlock()).getType() == Material.ANVIL || e.getClickedBlock().getType() == Material.CHIPPED_ANVIL || e.getClickedBlock().getType() == Material.DAMAGED_ANVIL)) {
                e.setCancelled(true);
                if (!p.hasPermission("drugfun.stamp")) {
                    p.sendMessage(Texts.nopermission);
                    return;
                }

                this.onStampDesign(p);
            }
        }

    }

    @EventHandler
    public void EcstacySwallow(final PlayerInteractEvent e10) {
        if (!e10.getPlayer().isSneaking() && (e10.getAction().equals(Action.RIGHT_CLICK_AIR) || e10.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            final Player pl = e10.getPlayer();
            if (pl.getItemInHand().hasItemMeta() && Objects.requireNonNull(pl.getItemInHand().getItemMeta()).hasLore() && HexChat.translateHexCodes( (String)pl.getItemInHand().getItemMeta().getLore().get(0), main).equals(ChatColor.WHITE + "Lysergic acid diethylamide, also known")) {
                if (!Objects.equals(e10.getHand(), EquipmentSlot.HAND)) {
                    return;
                }

                if (!e10.getPlayer().hasPermission("drugfun.consumelsd")) {
                    e10.getPlayer().sendMessage(HexChat.translateHexCodes( this.main.getConfig().getString("nopermstoconsume").replace("%drugs%", "LSD"), main));
                    e10.setCancelled(true);
                    return;
                }

                if (this.HCooldown.contains(e10.getPlayer())) {
                    return;
                }

                e10.setCancelled(true);
                pl.getInventory().getItemInHand().setAmount(pl.getInventory().getItemInHand().getAmount() - 1);
                pl.addPotionEffects(EffectsManager.LSDEffects);
                if (Main.bloods.containsKey(e10.getPlayer().getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(e10.getPlayer().getUniqueId())).setLsd(((DrugBloodManager)Main.bloods.get(e10.getPlayer().getUniqueId())).getLsd() + 10);
                }

                SlurManager.addSlurFinish(e10.getPlayer(), System.currentTimeMillis() + (long)(Main.lsdSlurTime * 1000.0D));
                SlurManager.setEffect(e10.getPlayer(), Main.lsdSlur);

                if (this.main.cs) {
                    pl.playSound(pl.getLocation(), Sound.ENTITY_GENERIC_EAT, 1.0F, 0.7F);
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {
                    public void run() {
                        if (LSD.this.main.acs) {
                            pl.playSound(pl.getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                        }

                    }
                }, 8L);
                pl.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("LSDSwallow")), main));
                Drugs.isOverdose(e10.getPlayer(), Drugs.LSD);
                this.HCooldown.add(e10.getPlayer());
                Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {
                    public void run() {
                        LSD.this.HCooldown.remove(e10.getPlayer());
                    }
                }, 20L);
            }
        }

    }

    @EventHandler
    public void MorningGloryPlant(BlockPlaceEvent e1) {
        if (e1.getPlayer().getItemInHand() != null && e1.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e1.getPlayer().getItemInHand().getItemMeta(), this.getMorningGloryPlantMeta())) {
            e1.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("MorningGloryPlant")), main));
            FileManager.putLocation(e1.getBlock().getLocation(), (long)((double)System.currentTimeMillis() + this.main.LSDTime * 60.0D * 1000.0D));
            if (this.main.dps) {
                e1.getPlayer().playSound(e1.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }

    }

    @EventHandler
    public void MorningGlorySeeds(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getHand() != EquipmentSlot.HAND) {
                return;
            }

            if (e.getClickedBlock().getType() != null && e.getClickedBlock().getType().equals(Material.CORNFLOWER) && FileManager.isValidLocation(e.getClickedBlock().getLocation())) {
                if (System.currentTimeMillis() >= FileManager.getLocationTime(e.getClickedBlock().getLocation())) {
                    Objects.requireNonNull(e.getClickedBlock().getLocation().getWorld()).dropItemNaturally(e.getClickedBlock().getLocation(), this.getMorningGloryPlantSeeds(this.main.LSDAmount));
                    if (((ItemStack)Main.LSDList.get(this.r.nextInt(Main.LSDList.size() - 1))).getType() == Material.DIAMOND) {
                        e.getClickedBlock().getLocation().getWorld().dropItemNaturally(e.getClickedBlock().getLocation(), this.getMorningGloryPlant(this.main.MorningGloryAmount));
                    }

                    FileManager.setTime(e.getClickedBlock().getLocation(), (long)((double)System.currentTimeMillis() + this.main.LSDTime * 60.0D * 1000.0D));
                    e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("MorningGloryHarvest")), main));
                    if (this.main.dhs) {
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.7F);
                    }
                } else {
                    e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("MorningGloryHarvestFail")), main));
                }
            }
        }

    }

    @EventHandler
    public void CancelSeedsPlace(BlockPlaceEvent e) {
        if (e.getPlayer().getInventory().getItemInHand() != null && e.getPlayer().getInventory().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getInventory().getItemInHand().getItemMeta(), this.getMorningGloryPlantSeedsMeta())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void receiveMorningGlory(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.CORNFLOWER && FileManager.isValidLocation(e.getBlock().getLocation())) {
            e.setDropItems(false);
            Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.getMorningGloryPlant(1));
            FileManager.removeLocation(e.getBlock().getLocation());
        }

    }

    @EventHandler
    public void CancellingHCL(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getInventory().getItemInHand().hasItemMeta() && Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.getLysergicAcidMeta())) {
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void CancelEthanol(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getInventory().getItemInHand().hasItemMeta() && Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.getEthanolMeta())) {
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void ChloroformAttack(PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked() instanceof Player) {
            final Player p2 = (Player)e.getRightClicked();
            Player p1 = e.getPlayer();
            if (e.getHand().equals(EquipmentSlot.HAND) && p1.getInventory().getItemInHand() != null && p1.getInventory().getItemInHand().hasItemMeta() && Objects.requireNonNull(p1.getInventory().getItemInHand().getItemMeta()).getDisplayName().contains("Chloroform")) {
                if (!this.main.getConfig().getBoolean("Chloroform")) {
                    return;
                }

                if (!p1.hasPermission("drugfun.consumelsd")) {
                    p1.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Chloroform")), main));
                    e.setCancelled(true);
                    return;
                }

                if (this.knockout.contains(p2)) {
                    p1.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("ChloroformFail")), main));
                }

                if (!this.knockout.contains(p2)) {
                    p1.getInventory().getItemInHand().setAmount(p1.getInventory().getItemInHand().getAmount() - 1);
                    this.knockout.add(p2);
                    p2.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, this.main.ChloroformDuration, 1000000));
                    p2.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, this.main.ChloroformDuration, 1000000));
                    p1.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("ChloroformAttacker")), main));
                    p2.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("ChloroformVictim")), main));
                    p1.getWorld().playSound(p2.getLocation(), Sound.ENTITY_FOX_SNIFF, 1.0F, 0.6F);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {
                        public void run() {
                            LSD.this.knockout.remove(p2);
                        }
                    }, this.main.ChloroformDuration.longValue());
                }
            }
        }

    }

    @EventHandler
    public void ChloroformDontMove(PlayerMoveEvent e) {
        if (this.knockout.contains(e.getPlayer())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void MorningGloryPlantWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.CORNFLOWER && FileManager.isValidLocation(block.getLocation())) {
            e.setCancelled(true);
            block.setType(Material.AIR);
            FileManager.removeLocation(block.getLocation());
            Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.getMorningGloryPlant(1));
        }

    }

    @EventHandler
    public void MorningGloryPlantExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.CORNFLOWER && FileManager.isValidLocation(block.getLocation())) {
                e.setCancelled(true);
                block.setType(Material.AIR);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.getMorningGloryPlant(1));
            }
        }

    }

    @EventHandler
    public void MorningGloryPlantRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.CORNFLOWER && FileManager.isValidLocation(block.getLocation())) {
                e.setCancelled(true);
                block.setType(Material.AIR);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.getMorningGloryPlant(1));
            }
        }
    }

    @EventHandler
    public void entityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.CORNFLOWER && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.getMorningGloryPlant(1));
            }
        }

    }

    public void setMorningGloryPlant(ItemStack item) {
        this.MorningGloryPlant = item;
    }

    public ItemStack getMorningGloryPlant(Integer amt) {
        this.MorningGloryPlant.setAmount(amt);
        return this.MorningGloryPlant;
    }

    public void setMorningGloryPlantMeta(ItemMeta item) {
        this.MorningGloryPlantMeta = item;
    }

    public ItemMeta getMorningGloryPlantMeta() {
        return this.MorningGloryPlantMeta;
    }

    public void setMorningGloryPlantSeeds(ItemStack item) {
        this.MorningGloryPlantSeeds = item;
    }

    public ItemStack getMorningGloryPlantSeeds(Integer amt) {
        this.MorningGloryPlantSeeds.setAmount(amt);
        return this.MorningGloryPlantSeeds;
    }

    public void setMorningGloryPlantSeedsMeta(ItemMeta item) {
        this.MorningGloryPlantSeedsMeta = item;
    }

    public ItemMeta getMorningGloryPlantSeedsMeta() {
        return this.MorningGloryPlantSeedsMeta;
    }

    public void setLysergicAcid(ItemStack item) {
        this.LysergicAcid = item;
    }

    public ItemStack getLysergicAcid(Integer amt) {
        this.LysergicAcid.setAmount(amt);
        return this.LysergicAcid;
    }

    public void setLysergicAcidMeta(ItemMeta item) {
        this.LysergicAcidMeta = item;
    }

    public ItemMeta getLysergicAcidMeta() {
        return this.LysergicAcidMeta;
    }

    public void setChloroform(ItemStack item) {
        this.Chloroform = item;
    }

    public ItemStack getChloroform(Integer amt) {
        this.Chloroform.setAmount(amt);
        return this.Chloroform;
    }

    public void setChloroformMeta(ItemMeta item) {
        this.ChloroformMeta = item;
    }

    public ItemMeta getChloroformMeta() {
        return this.ChloroformMeta;
    }

    public void setEthanol(ItemStack item) {
        this.Ethanol = item;
    }

    public ItemStack getEthanol(Integer amt) {
        this.Ethanol.setAmount(amt);
        return this.Ethanol;
    }

    public void setEthanolMeta(ItemMeta item) {
        this.EthanolMeta = item;
    }

    public ItemMeta getEthanolMeta() {
        return this.EthanolMeta;
    }

    public void setAcid(ItemStack item) {
        this.Acid = item;
    }

    public ItemStack getAcid(Integer amt) {
        this.Acid.setAmount(amt);
        return this.Acid;
    }

    public void setAcidMeta(ItemMeta item) {
        this.AcidMeta = item;
    }

    public ItemMeta getAcidMeta() {
        return this.AcidMeta;
    }
}
