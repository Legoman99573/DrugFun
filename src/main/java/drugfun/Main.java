package drugfun;

import java.util.*;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    private static Main main;

    public static Main getMain() {
        return main;
    }

    static HashMap<UUID, DrugBloodManager> bloods = new HashMap();
    static int odcocaine;
    static int odheroin;
    static int odecstasy;
    static int odlsd;
    static int odmeth;
    static int odshrooms;
    static int odweed;
    static String overdoseMsg;
    static SlurEffects cocaineSlur;
    static SlurEffects ecstasySlur;
    static SlurEffects heroinSlur;
    static SlurEffects lsdSlur;
    static SlurEffects methSlur;
    static SlurEffects shroomsSlur;
    static SlurEffects weedSlur;
    static double cocaineSlurTime;
    static double ecstasySlurTime;
    static double heroinSlurTime;
    static double lsdSlurTime;
    static double methSlurTime;
    static double shroomsSlurTime;
    static double weedSlurTime;
    static HashMap<UUID, Long> slurs = new HashMap();
    static HashMap<UUID, SlurEffects> slurseffect = new HashMap();
    static String slurtextformat;
    private FileManager fileManager;
    static ArrayList<Location> drugdealerLocs = new ArrayList();
    Integer ChloroformDuration = this.getConfig().getInt("ChloroformDuration") * 20;
    Double HeroinTime = this.getConfig().getDouble("HarvestingTime.Opiumpoppy");
    Double WeedTime = this.getConfig().getDouble("HarvestingTime.Weed");
    Double CocaTime = this.getConfig().getDouble("HarvestingTime.Cocaleaf");
    Double LSDTime = this.getConfig().getDouble("HarvestingTime.MorningGlory");
    Integer HeroinSeedsAmount = this.getConfig().getInt("HarvestingAmount.Opiumpoppyseedpods");
    Integer WeedAmount = this.getConfig().getInt("HarvestingAmount.Weed");
    Integer CocaLeafAmount = this.getConfig().getInt("HarvestingAmount.Cocaleaf");
    Integer EphedrineAmount = this.getConfig().getInt("HarvestingAmount.Ephedrine");
    Integer SbarkAmount = this.getConfig().getInt("HarvestingAmount.SassafrasBark");
    Integer LSDAmount = this.getConfig().getInt("HarvestingAmount.MorningGloryPlantSeeds");
    Integer OpiumSmelted = this.getConfig().getInt("AmountOfSmelted.Opium");
    Integer SafroleSmelted = this.getConfig().getInt("AmountOfSmelted.Safrole");
    Integer LysergicAcidSmelted = this.getConfig().getInt("AmountOfSmelted.LysergicAcid");
    Boolean cs = this.getConfig().getBoolean("DRUG_CONSUMPTION_SOUND");
    Boolean acs = this.getConfig().getBoolean("DRUG_CONSUMPTION_AFTER_SOUND");
    Boolean dps = this.getConfig().getBoolean("DRUG_PLANT_SOUND");
    Boolean dhs = this.getConfig().getBoolean("DRUG_HARVEST_SOUND");
    Double cocaplantRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.Cocaine");
    Integer cocaplantChance;
    Double weedRaw;
    Integer weedChance;
    Double heroinRaw;
    Integer heroinChance;
    Double methRaw;
    Integer methChance;
    Double MorningGloryRaw;
    Integer MorningGloryChance;
    Double sassafrasRaw;
    Integer sassafrasChance;
    Double mercuryRaw;
    Integer mercuryChance;
    Integer cocaplantAmount;
    Integer weedplantAmount;
    Integer heroinplantAmount;
    Integer methplantAmount;
    Integer MorningGloryAmount;
    Integer sassafrasplantAmount;
    String mercuryAmount;
    static String dfPrefix;
    static ArrayList<ItemStack> cocaineList;
    static ArrayList<ItemStack> weedList;
    static ArrayList<ItemStack> heroinList;
    static ArrayList<ItemStack> methList;
    static ArrayList<ItemStack> sassafrasList;
    static ArrayList<ItemStack> mercuryList;
    static ArrayList<ItemStack> LSDList;
    public static Economy economy;
    DrugShop ds;
    LSD lsd;
    Items i;

    static {
        dfPrefix = "" + ChatColor.DARK_RED + ChatColor.BOLD + "[" + ChatColor.RED + ChatColor.BOLD + "DrugFun" + ChatColor.DARK_RED + ChatColor.BOLD + "] ";
        economy = null;
    }

    public Main() {
        main = this;
        this.cocaplantChance = this.cocaplantRaw.intValue();
        this.weedRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.Weed");
        this.weedChance = this.weedRaw.intValue();
        this.heroinRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.Heroin");
        this.heroinChance = this.heroinRaw.intValue();
        this.methRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.Meth");
        this.methChance = this.methRaw.intValue();
        this.MorningGloryRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.MorningGlory");
        this.MorningGloryChance = this.MorningGloryRaw.intValue();
        this.sassafrasRaw = this.getConfig().getDouble("ChanceOfPlantDroppingFromHarvest.Sassafras");
        this.sassafrasChance = this.sassafrasRaw.intValue();
        this.mercuryRaw = this.getConfig().getDouble("Mercury.ChanceOfDrop");
        this.mercuryChance = this.mercuryRaw.intValue();
        this.cocaplantAmount = this.getConfig().getInt("AmountOfPlantsDropped.Cocaine");
        this.weedplantAmount = this.getConfig().getInt("AmountOfPlantsDropped.Weed");
        this.heroinplantAmount = this.getConfig().getInt("AmountOfPlantsDropped.Heroin");
        this.methplantAmount = this.getConfig().getInt("AmountOfPlantsDropped.Meth");
        this.MorningGloryAmount = this.getConfig().getInt("AmountOfPlantsDropped.MorningGlory");
        this.sassafrasplantAmount = this.getConfig().getInt("AmountOfPlantsDropped.Sassafras");
        this.mercuryAmount = this.getConfig().getString("Mercury.Amount");
        this.ds = new DrugShop(this);
        this.lsd = new LSD(this);
        this.i = new Items();
    }

    @Override
    public void onEnable() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            getLogger().severe( "Dependency Vault couldn't be found.");
            getLogger().severe( "You can grab latest version of Vault on https://ci.md-5.net/job/Vault.");
            getLogger().severe( "Disabling Plugin");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (getServer().getPluginManager().getPlugin("UltimateTimber") != null) {
            getLogger().info( "Dependency UltimateTimber found. Hooking into plugin.");
            Bukkit.getPluginManager().registerEvents(new UltumateTreesDrugTreeFall(), this);
        }
        this.getConfig().options().copyDefaults(true);
        Bukkit.getPluginManager().registerEvents(new DrugShop(this), this);
        Bukkit.getPluginManager().registerEvents(new LSD(this), this);
        Bukkit.getPluginManager().registerEvents(new Cocaine(this), this);
        Bukkit.getPluginManager().registerEvents(new Heroin(this), this);
        Bukkit.getPluginManager().registerEvents(new Weed(this), this);
        Bukkit.getPluginManager().registerEvents(new Methamphetamine(this), this);
        Bukkit.getPluginManager().registerEvents(new Ecstacy(this), this);
        Bukkit.getPluginManager().registerEvents(new Syringe(this), this);
        Bukkit.getPluginManager().registerEvents(new Shrooms(this), this);
        Bukkit.getPluginManager().registerEvents(new SlurManager(this), this);
        new RecipeManager(this, new Items(), new LSD(this), this.getServer().getPluginManager().getPlugin("DrugFun"));
        this.getCommand("drugfun").setExecutor(new DrugFunMenu(this));
        this.getCommand("drugfun").setTabCompleter(new DrugFunTabComplete());
        Bukkit.getPluginManager().registerEvents(this, this);
        Methamphetamine.Recipes();
        Syringe.SyringeCraft();
        Shrooms shrooms = new Shrooms(this);
        shrooms.onShroomsCraft();
        this.setupEconomy();
        this.dropChances();
        this.saveDefaultConfig();
        this.fileManager = new FileManager(this);
        new EffectsManager(this);
        new Texts(this);
        this.fileManager.onSaveDataReapeating(this);
        Iterator var3 = Bukkit.getOnlinePlayers().iterator();

        while(var3.hasNext()) {
            Player p = (Player)var3.next();
            addBlood(p.getUniqueId());
        }

        odcocaine = this.getConfig().getInt("DrugOverdose.Cocaine");
        odheroin = this.getConfig().getInt("DrugOverdose.Heroin");
        odecstasy = this.getConfig().getInt("DrugOverdose.Ecstasy");
        odlsd = this.getConfig().getInt("DrugOverdose.LSD");
        odmeth = this.getConfig().getInt("DrugOverdose.Meth");
        odshrooms = this.getConfig().getInt("DrugOverdose.Shrooms");
        odweed = this.getConfig().getInt("DrugOverdose.Weed");
        overdoseMsg = HexChat.translateHexCodes(this.getConfig().getString("overdoseMsg"), this);
        this.ReduceDrugAmounts();
        cocaineSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Cocaine.DrugSlur").toUpperCase());
        ecstasySlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Ecstasy.DrugSlur").toUpperCase());
        heroinSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Heroin.DrugSlur").toUpperCase());
        lsdSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.LSD.DrugSlur").toUpperCase());
        methSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Meth.DrugSlur").toUpperCase());
        shroomsSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Shrooms.DrugSlur").toUpperCase());
        weedSlur = SlurEffects.valueOf(this.getConfig().getString("SlurEffects.Weed.DrugSlur").toUpperCase());
        cocaineSlurTime = this.getConfig().getDouble("SlurEffects.Cocaine.timeOfSlur");
        ecstasySlurTime = this.getConfig().getDouble("SlurEffects.Ecstasy.timeOfSlur");
        heroinSlurTime = this.getConfig().getDouble("SlurEffects.Heroin.timeOfSlur");
        lsdSlurTime = this.getConfig().getDouble("SlurEffects.LSD.timeOfSlur");
        methSlurTime = this.getConfig().getDouble("SlurEffects.Meth.timeOfSlur");
        shroomsSlurTime = this.getConfig().getDouble("SlurEffects.Shrooms.timeOfSlur");
        weedSlurTime = this.getConfig().getDouble("SlurEffects.Weed.timeOfSlur");
        slurtextformat = this.getConfig().getString("SlurTextFormat");

        if (Main.getServerVersion().getVersionInt() < VersionEnums.VERSION_116.getVersionInt()) {
            Bukkit.getLogger().warning( "Your server seems to not be running 1.16 or higher. Note that there will be no hex support");
        } else if (Main.getServerVersion().getVersionInt() < VersionEnums.VERSION_114.getVersionInt()) {
            Bukkit.getLogger().severe( "Your server seems to not be running" + Main.getServerVersion().getVersionInt() + ". This version is not supported. Disabling plugin.");
        } else {
            Bukkit.getLogger().info( "Server is running 1.16+. Hex support should work.");
        }
    }

    public static VersionEnums getServerVersion() {
        if (Bukkit.getServer().getVersion().contains("1.8")) {
            return VersionEnums.VERSION_18;
        } else if (Bukkit.getServer().getVersion().contains("1.9")) {
            return VersionEnums.VERSION_19;
        } else if (Bukkit.getServer().getVersion().contains("1.10")) {
            return VersionEnums.VERSION_110;
        } else if (Bukkit.getServer().getVersion().contains("1.11")) {
            return VersionEnums.VERSION_111;
        } else if (Bukkit.getServer().getVersion().contains("1.12")) {
            return VersionEnums.VERSION_112;
        } else if (Bukkit.getServer().getVersion().contains("1.13")) {
            return VersionEnums.VERSION_113;
        } else if (Bukkit.getServer().getVersion().contains("1.14")) {
            return VersionEnums.VERSION_114;
        } else if (Bukkit.getServer().getVersion().contains("1.15")) {
            return VersionEnums.VERSION_115;
        } else {
            return Bukkit.getServer().getVersion().contains("1.16") ? VersionEnums.VERSION_116 : VersionEnums.OTHER_VERSION;
        }
    }

    static void addBlood(UUID uuid) {
        if (!bloods.containsKey(uuid)) {
            bloods.put(uuid, new DrugBloodManager());
        }

    }

    static void getInfo(Player p, Player target) {
        p.sendMessage(Texts.prefix + HexChat.translateHexCodes("&#6F57B2" + target.getName() + "'s &#57B29CBlood test results", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7ECocaine > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getCocaine() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7EEcstasy > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getEcstasy() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7ELSD > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getLsd() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7EWeed > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getWeed() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7EHeroin > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getHeroin() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7EShrooms > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getShrooms() / 50.0F + "&#9D3F3F %", main));
        p.sendMessage(HexChat.translateHexCodes("&#6F57B2 - &#5D6D7EMethamphetamine > &#C0C01C" + (float)((DrugBloodManager)bloods.get(target.getUniqueId())).getMeth() / 50.0F + "&#9D3F3F %", main));
    }

    public void ReduceDrugAmounts() {
        double reduc = this.getConfig().getDouble("ReductionUpdateTime");
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                Iterator var2 = Bukkit.getOnlinePlayers().iterator();

                while(var2.hasNext()) {
                    Player p = (Player)var2.next();
                    UUID uuid = p.getUniqueId();
                    ((DrugBloodManager)Main.bloods.get(uuid)).setCocaine((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getCocaine() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Cocaine")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setEcstasy((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getEcstasy() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Ecstasy")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setHeroin((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getHeroin() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Heroin")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setLsd((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getLsd() * Main.this.getConfig().getDouble("DrugReductionMultipliers.LSD")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setMeth((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getMeth() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Meth")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setShrooms((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getShrooms() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Shrooms")));
                    ((DrugBloodManager)Main.bloods.get(uuid)).setWeed((int)((double)((DrugBloodManager)Main.bloods.get(uuid)).getWeed() * Main.this.getConfig().getDouble("DrugReductionMultipliers.Weed")));
                }

            }
        }, (long)(reduc * 60.0D * 20.0D), (long)(reduc * 60.0D * 20.0D));
    }

    static void clearBlood(Player p) {
        UUID uuid = p.getUniqueId();
        ((DrugBloodManager)bloods.get(uuid)).setCocaine(0);
        ((DrugBloodManager)bloods.get(uuid)).setEcstasy(0);
        ((DrugBloodManager)bloods.get(uuid)).setHeroin(0);
        ((DrugBloodManager)bloods.get(uuid)).setLsd(0);
        ((DrugBloodManager)bloods.get(uuid)).setMeth(0);
        ((DrugBloodManager)bloods.get(uuid)).setShrooms(0);
        ((DrugBloodManager)bloods.get(uuid)).setWeed(0);
    }

    @EventHandler
    public void JoinAdd(PlayerJoinEvent e) {
        addBlood(e.getPlayer().getUniqueId());
    }

    @Override
    public void onDisable() {
        FileManager.saveOnDisable();
    }

    public void dropChances() {
        cocaineList = new ArrayList();
        weedList = new ArrayList();
        heroinList = new ArrayList();
        methList = new ArrayList();
        sassafrasList = new ArrayList();
        mercuryList = new ArrayList();
        LSDList = new ArrayList();

        int i;
        for(i = 0; i < 100; ++i) {
            if (i < this.cocaplantChance) {
                cocaineList.add(new ItemStack(Material.DIAMOND));
            } else {
                cocaineList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.weedChance) {
                weedList.add(new ItemStack(Material.DIAMOND));
            } else {
                weedList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.heroinChance) {
                heroinList.add(new ItemStack(Material.DIAMOND));
            } else {
                heroinList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.methChance) {
                methList.add(new ItemStack(Material.DIAMOND));
            } else {
                methList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.sassafrasChance) {
                sassafrasList.add(new ItemStack(Material.DIAMOND));
            } else {
                sassafrasList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.mercuryChance) {
                mercuryList.add(new ItemStack(Material.DIAMOND));
            } else {
                mercuryList.add(new ItemStack(Material.BARRIER));
            }
        }

        for(i = 0; i < 100; ++i) {
            if (i < this.MorningGloryChance) {
                LSDList.add(new ItemStack(Material.DIAMOND));
            } else {
                LSDList.add(new ItemStack(Material.BARRIER));
            }
        }

    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = this.getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            economy = (Economy)economyProvider.getProvider();
        }

        return economy != null;
    }

    @EventHandler
    public void onOpenDrugShop(PlayerInteractEntityEvent e) {
        if (e.getHand() == EquipmentSlot.HAND && !(e.getRightClicked() instanceof Player) && e.getRightClicked().getName().contains(Texts.drugdealername)) {
            if (e.getPlayer().hasPermission("drugfun.shopdealer")) {
                e.setCancelled(true);
                this.ds.openMainMenu(e.getPlayer());
                Player p = e.getPlayer();
                Objects.requireNonNull(p.getLocation().getWorld()).playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            } else {
                e.setCancelled(true);
                e.getPlayer().sendMessage(Texts.nopermission);
            }
        }
    }

    @EventHandler
    public void onFightingWithDrugdealer(EntityDamageByEntityEvent event) {
        if(event.getEntityType() == EntityType.VILLAGER && event.getEntity().getName().contains(Texts.drugdealername)) {
            event.setCancelled(true);
        }
        if(event.getEntityType() == EntityType.WANDERING_TRADER && event.getEntity().getName().contains(Texts.drugdealername)) {
            event.setCancelled(true);
        }
        if(event.getEntityType() == EntityType.PIGLIN && event.getEntity().getName().contains(Texts.drugdealername)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void DrugPlantWildFire(BlockBurnEvent e) {
        if (FileManager.isValidLocation(e.getBlock().getLocation())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public boolean onDrugPlaceBlockOnAccident(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        Block block = e.getBlock();
        if (FileManager.isValidLocation(block.getLocation())) {
            if (e.getBlockReplacedState().getType().equals(Material.LARGE_FERN)) {
                e.setCancelled(true);
                return true;
            }
            if (e.getBlockReplacedState().getType().equals(Material.TALL_GRASS)) {
                e.setCancelled(true);
                return true;
            }

            // Ephedra Plant in hand so we dont remove the block by accident
            if (e.getBlock().getLocation().add(0.0D, -1.0D, 0.0D).getBlock().getType().equals(Material.FARMLAND) && Objects.equals(e.getItemInHand().getItemMeta(), this.i.getEsmeta())) {
                return true;
            }

            // Sassafras Plant in hand so we dont remove the block by accident
            if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta() && e.getPlayer().getInventory().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getInventory().getItemInHand().getItemMeta(), this.i.getSsmeta())) {
                return true;
            }

            // Poppy Plant in hand so we dont remove the block by accident
            if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getItemInHand().getItemMeta(), this.i.getOpitemmeta())) {
                return true;
            }

            // Weed in hand so we dont remove the block by accident
            if (p.getItemInHand() != null && p.getItemInHand().hasItemMeta() && Objects.equals(p.getItemInHand().getItemMeta(), this.i.getWsitemmeta())) {
                return true;
            }

            // Cocaine in hand so we dont remove the block by accident
            if (p.getItemInHand() != null && p.getItemInHand().hasItemMeta() && Objects.equals(p.getItemInHand().getItemMeta(), i.getCpitemmeta())) {
                return true;
            }

            // Morning Glory Plant in hand so we dont remove the block by accident
            if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta() && Objects.equals(e.getPlayer().getItemInHand().getItemMeta(), this.lsd.getMorningGloryPlantMeta())) {
                return true;
            }


            // This means that the block placed isnt even a valid drug location. Best to remove it from locations.yml
            p.sendMessage(Texts.prefix + ChatColor.RED + "Drug location  x: " + e.getBlockReplacedState().getLocation().getBlockX() + " y: " + e.getBlockReplacedState().getLocation().getBlockY() + " z: " + e.getBlockReplacedState().getLocation().getBlockZ() + " is invalid. Removing this location from memory.");
            p.sendMessage(Texts.prefix + ChatColor.RED + "If you placed the drug down and this message pops up afterwards, then this is a bug.");
            FileManager.removeLocation(e.getBlockReplacedState().getLocation());
            return false;
        }

        return false;
     }

    public void DrugMenu(Player p) {
        Inventory dmenu = Bukkit.createInventory((InventoryHolder)null, 36, HexChat.translateHexCodes(Objects.requireNonNull(this.getConfig().getString("ItemMenuName")), this));
        dmenu.setItem(0, this.i.getCitem(1));
        dmenu.setItem(1, this.i.getClitem(1));
        dmenu.setItem(2, this.i.getCpitem(1));
        dmenu.setItem(3, this.i.getHitem(1));
        dmenu.setItem(4, this.i.getMitem(1));
        dmenu.setItem(5, this.i.getOitem(1));
        dmenu.setItem(6, this.i.getOpitem(1));
        dmenu.setItem(7, this.i.getOpspitem(1));
        dmenu.setItem(8, this.i.getWitem(1));
        dmenu.setItem(9, this.i.getWsitem(1));
        dmenu.setItem(10, this.i.getBluntitem(1));
        dmenu.setItem(11, this.i.getMethitem(1));
        dmenu.setItem(12, this.i.getHclitem(1));
        dmenu.setItem(13, this.i.getIitem(1));
        dmenu.setItem(14, this.i.getPitem(1));
        dmenu.setItem(15, this.i.getEitem(1));
        dmenu.setItem(16, this.i.getEsitem(1));
        dmenu.setItem(17, this.i.getSyrItem(1));
        dmenu.setItem(18, this.i.getShroomItem(1));
        dmenu.setItem(19, this.i.getEcitem(1));
        dmenu.setItem(20, this.i.getMercury(1));
        dmenu.setItem(21, this.i.getMethylChloride(1));
        dmenu.setItem(22, this.i.getSafrole(1));
        dmenu.setItem(23, this.i.getSbark(1));
        dmenu.setItem(24, this.i.getSsitem(1));
        dmenu.setItem(25, this.lsd.getAcid(1));
        dmenu.setItem(26, this.lsd.getChloroform(1));
        dmenu.setItem(27, this.lsd.getEthanol(1));
        dmenu.setItem(28, this.lsd.getLysergicAcid(1));
        dmenu.setItem(29, this.lsd.getMorningGloryPlantSeeds(1));
        dmenu.setItem(30, this.lsd.getMorningGloryPlant(1));
        dmenu.setItem(35, this.i.getExititem());
        p.openInventory(dmenu);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player p = (Player)e.getWhoClicked();
        if (e.getView().getTitle().equals(HexChat.translateHexCodes( Objects.requireNonNull(this.getConfig().getString("ItemMenuName")), this)) && e.getCurrentItem() != null) {
            switch(e.getRawSlot()) {
                case 0:
                    p.getInventory().addItem(new ItemStack[]{this.i.getCitem(64)});
                    e.setCancelled(true);
                    break;
                case 1:
                    p.getInventory().addItem(new ItemStack[]{this.i.getClitem(64)});
                    e.setCancelled(true);
                    break;
                case 2:
                    p.getInventory().addItem(new ItemStack[]{this.i.getCpitem(64)});
                    e.setCancelled(true);
                    break;
                case 3:
                    p.getInventory().addItem(new ItemStack[]{this.i.getHitem(64)});
                    e.setCancelled(true);
                    break;
                case 4:
                    p.getInventory().addItem(new ItemStack[]{this.i.getMitem(64)});
                    e.setCancelled(true);
                    break;
                case 5:
                    p.getInventory().addItem(new ItemStack[]{this.i.getOitem(64)});
                    e.setCancelled(true);
                    break;
                case 6:
                    p.getInventory().addItem(new ItemStack[]{this.i.getOpitem(64)});
                    e.setCancelled(true);
                    break;
                case 7:
                    p.getInventory().addItem(new ItemStack[]{this.i.getOpspitem(64)});
                    e.setCancelled(true);
                    break;
                case 8:
                    p.getInventory().addItem(new ItemStack[]{this.i.getWitem(64)});
                    e.setCancelled(true);
                    break;
                case 9:
                    p.getInventory().addItem(new ItemStack[]{this.i.getWsitem(64)});
                    e.setCancelled(true);
                    break;
                case 10:
                    p.getInventory().addItem(new ItemStack[]{this.i.getBluntitem(64)});
                    e.setCancelled(true);
                    break;
                case 11:
                    p.getInventory().addItem(new ItemStack[]{this.i.getMethitem(64)});
                    e.setCancelled(true);
                    break;
                case 12:
                    p.getInventory().addItem(new ItemStack[]{this.i.getHclitem(64)});
                    e.setCancelled(true);
                    break;
                case 13:
                    p.getInventory().addItem(new ItemStack[]{this.i.getIitem(64)});
                    e.setCancelled(true);
                    break;
                case 14:
                    p.getInventory().addItem(new ItemStack[]{this.i.getPitem(64)});
                    e.setCancelled(true);
                    break;
                case 15:
                    p.getInventory().addItem(new ItemStack[]{this.i.getEitem(64)});
                    e.setCancelled(true);
                    break;
                case 16:
                    p.getInventory().addItem(new ItemStack[]{this.i.getEsitem(64)});
                    e.setCancelled(true);
                    break;
                case 17:
                    p.getInventory().addItem(new ItemStack[]{this.i.getSyrItem(64)});
                    e.setCancelled(true);
                    break;
                case 18:
                    p.getInventory().addItem(new ItemStack[]{this.i.getShroomItem(64)});
                    e.setCancelled(true);
                    break;
                case 19:
                    p.getInventory().addItem(new ItemStack[]{this.i.getEcitem(64)});
                    e.setCancelled(true);
                    break;
                case 20:
                    p.getInventory().addItem(new ItemStack[]{this.i.getMercury(64)});
                    e.setCancelled(true);
                    break;
                case 21:
                    p.getInventory().addItem(new ItemStack[]{this.i.getMethylChloride(64)});
                    e.setCancelled(true);
                    break;
                case 22:
                    p.getInventory().addItem(new ItemStack[]{this.i.getSafrole(64)});
                    e.setCancelled(true);
                    break;
                case 23:
                    p.getInventory().addItem(new ItemStack[]{this.i.getSbark(64)});
                    e.setCancelled(true);
                    break;
                case 24:
                    p.getInventory().addItem(new ItemStack[]{this.i.getSsitem(64)});
                    e.setCancelled(true);
                    break;
                case 25:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getAcid(64)});
                    e.setCancelled(true);
                    break;
                case 26:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getChloroform(64)});
                    e.setCancelled(true);
                    break;
                case 27:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getEthanol(64)});
                    e.setCancelled(true);
                    break;
                case 28:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getLysergicAcid(64)});
                    e.setCancelled(true);
                    break;
                case 29:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getMorningGloryPlantSeeds(64)});
                    e.setCancelled(true);
                    break;
                case 30:
                    p.getInventory().addItem(new ItemStack[]{this.lsd.getMorningGloryPlant(64)});
                    e.setCancelled(true);
                case 31:
                case 32:
                case 33:
                case 34:
                default:
                    break;
                case 35:
                    p.closeInventory();
                    e.setCancelled(true);
            }
        }

    }
}
