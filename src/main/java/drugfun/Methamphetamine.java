package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.Ageable;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class Methamphetamine implements Listener {
    public ArrayList<Player> MCooldown = new ArrayList();
    public ArrayList<Player> MSyrCooldown = new ArrayList();
    public ArrayList<Player> ECooldown = new ArrayList();
    private static Main main;
    Items i = new Items();
    Items items = new Items();
    Random r = new Random();

    @EventHandler
    public void SyringeUseMeth(final PlayerInteractEvent e) {
        if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && e.getHand().equals(EquipmentSlot.HAND) && e.getPlayer().getItemInHand().hasItemMeta() && e.getPlayer().getItemInHand().getItemMeta().equals(this.i.getSyrMethMeta()) && !this.MSyrCooldown.contains(e.getPlayer())) {

            if (!e.getPlayer().hasPermission("drugfun.consumemeth")) {
                e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Meth")), main));
                e.setCancelled(true);
                return;
            }

            e.setCancelled(true);
            Player p = e.getPlayer();
            p.addPotionEffects(EffectsManager.MethEffects);
            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
            e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("InjectMeth")), main));
            this.MSyrCooldown.add(e.getPlayer());
            if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setMeth(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getMeth() + 10);
            }

            SlurManager.addSlurFinish(e.getPlayer(), System.currentTimeMillis() + (long)(Main.methSlurTime * 1000.0D));
            SlurManager.setEffect(e.getPlayer(), Main.methSlur);
            Drugs.isOverdose(e.getPlayer(), Drugs.METH);
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    if (Methamphetamine.main.acs) {
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                    }

                }
            }, 8L);
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    Methamphetamine.this.MSyrCooldown.remove(e.getPlayer());
                }
            }, 20L);
        }

    }

    @EventHandler
    public void InjectIntoEntitesH(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
            Player p = (Player)e.getDamager();
            LivingEntity et = (LivingEntity)e.getEntity();
            if (p.getInventory().getItemInHand().hasItemMeta() && p.getInventory().getItemInHand().getItemMeta().equals(this.i.getSyrMethMeta())) {
                if (!p.hasPermission("drugfun.consumemeth")) {
                    p.sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Meth")), main));
                    e.setCancelled(true);
                    return;
                }

                p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
                e.setCancelled(true);
                p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.6F);
                et.addPotionEffects(EffectsManager.MethEffects);
                p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("InjectMethAttacker")), main));
                et.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("InjectMethVictim")), main));
                if (Main.bloods.containsKey(et.getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(et.getUniqueId())).setMeth(((DrugBloodManager)Main.bloods.get(et.getUniqueId())).getMeth() + 10);
                }

                Drugs.isOverdose((Player)et, Drugs.METH);
                SlurManager.addSlurFinish((Player)et, System.currentTimeMillis() + (long)(Main.methSlurTime * 1000.0D));
                SlurManager.setEffect((Player)et, Main.methSlur);
            }
        }

    }

    @EventHandler
    public void CancellingDrops(ItemSpawnEvent e) {
        if ((e.getEntity().getItemStack().getType().equals(Material.WHEAT_SEEDS) || e.getEntity().getItemStack().getType().equals(Material.WHEAT)) && e.getLocation().add(-2000000.0D, 0.0D, 0.0D).getBlock().getType().equals(Material.BARRIER)) {
            e.setCancelled(true);
        }

    }

    public Methamphetamine(Main main) {
        Methamphetamine.main = main;
    }

    @EventHandler
    public void EphedraPlant(BlockPlaceEvent e) {
        if (e.getBlock().getLocation().add(0.0D, -1.0D, 0.0D).getBlock().getType().equals(Material.FARMLAND) && e.getItemInHand().hasItemMeta() && Objects.equals(e.getItemInHand().getItemMeta(), this.i.getEsmeta())) {
            e.getBlock().setType(Material.WHEAT);
            FileManager.putLocation(e.getBlock().getLocation(), 0L);
            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("EphedraPlant")), main));
            if (main.dps) {
                e.getPlayer().playSound(Objects.requireNonNull(e.getPlayer().getPlayer()).getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }

        if (!e.getBlock().getLocation().add(0.0D, -1.0D, 0.0D).getBlock().getType().equals(Material.FARMLAND) && e.getItemInHand().hasItemMeta() && Objects.equals(e.getItemInHand().getItemMeta(), this.i.getEsmeta())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("EphedraPlantError")), main));
        }

    }

    @EventHandler
    public void HarvestingEphedrine(BlockBreakEvent e) {
        if (e.getBlock().getType().equals(Material.WHEAT) && FileManager.isValidLocation(e.getBlock().getLocation())) {
            Block b = e.getBlock();
            Ageable age = (Ageable)b.getBlockData();
            if (age.getAge() == 7) {
                e.setDropItems(false);
                b.getWorld().dropItemNaturally(b.getLocation(), this.i.getEitem(main.EphedrineAmount));
                if (((ItemStack)Main.methList.get(this.r.nextInt(Main.methList.size() - 1))).getType() == Material.DIAMOND) {
                    Objects.requireNonNull(e.getBlock().getLocation().getWorld()).dropItemNaturally(e.getBlock().getLocation(), this.items.getEsitem(main.methplantAmount));
                }

                FileManager.removeLocation(e.getBlock().getLocation());
                e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("EphedraHarvest")), main));
                if (main.dhs) {
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.7F);
                }
            }

            if (age.getAge() < 7) {
                e.setDropItems(false);
                FileManager.removeLocation(e.getBlock().getLocation());
            }
        }

    }

    @EventHandler
    public void PurifyWater(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && Objects.equals(e.getHand(), EquipmentSlot.HAND)) {
            Block b = e.getClickedBlock();
            Location l = b.getLocation();
            if (b.getType().equals(Material.CAULDRON) && l.add(0.0D, 1.0D, 0.0D).getBlock().getType().equals(Material.HOPPER)) {
                BlockState bs = b.getState();
                switch(b.getData()) {
                    case 0:
                        if (!e.getPlayer().getItemInHand().getType().equals(Material.WATER_BUCKET)) {
                            e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes(main.getConfig().getString("PhosphorusInfo"), main));
                        }
                        break;
                    case 1:
                    case 3:
                    case 2:
                        e.getPlayer().getInventory().addItem(new ItemStack[]{this.i.getPitem(1)});
                        bs.setRawData((byte)(b.getData() - 1));
                        bs.update(true);
                        e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( main.getConfig().getString("ReceivePhosphorus"), main));
                        e.setCancelled(true);
                        break;
                    default:
                        return;
                }
            }
        }

    }

    static void Recipes() {
        Items i = new Items();
        NamespacedKey iodine = new NamespacedKey(main, "IodineCraft");
        ShapedRecipe iodinerecipe = new ShapedRecipe(iodine, i.getIitem(1));
        iodinerecipe.shape(new String[]{"  $", " $ ", "$  "});
        iodinerecipe.setIngredient('$', Material.DRIED_KELP);
        main.getServer().addRecipe(iodinerecipe);
        NamespacedKey hydrochloric = new NamespacedKey(main, "HydrochloricAcid");
        ShapedRecipe hydrochloricrecipe = new ShapedRecipe(hydrochloric, i.getHclitem(1));
        hydrochloricrecipe.shape(new String[]{"£ £", " $ ", "£ £"});
        hydrochloricrecipe.setIngredient('£', Material.BLAZE_POWDER);
        hydrochloricrecipe.setIngredient('$', Material.WATER_BUCKET);
        main.getServer().addRecipe(hydrochloricrecipe);
    }

    @EventHandler
    public void SnortMeth(final PlayerInteractEvent e) {
        if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && e.getPlayer().getItemInHand().hasItemMeta()) {
            if (!Objects.equals(e.getPlayer().getItemInHand().getItemMeta(), this.i.getMethmeta())) {
                return;
            }

            if (!e.getPlayer().hasPermission("drugfun.consumemeth")) {
                e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Meth")), main));
                e.setCancelled(true);
                return;
            }

            if (Objects.equals(e.getHand(), EquipmentSlot.HAND) && !this.MCooldown.contains(e.getPlayer())) {
                e.setCancelled(true);
                e.getPlayer().getInventory().getItemInHand().setAmount(e.getPlayer().getInventory().getItemInHand().getAmount() - 1);
                Player p = e.getPlayer();
                p.addPotionEffects(EffectsManager.MethEffects);
                if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                    ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setMeth(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getMeth() + 10);
                }

                if (main.cs) {
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_GENERIC_DRINK, 1.0F, 0.7F);
                }

                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        if (Methamphetamine.main.acs) {
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                        }

                    }
                }, 8L);
                e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("SnortMeth")), main));
                Drugs.isOverdose(e.getPlayer(), Drugs.METH);
                SlurManager.addSlurFinish((Player)p, System.currentTimeMillis() + (long)(Main.methSlurTime * 1000.0D));
                SlurManager.setEffect((Player)p, Main.methSlur);
                this.MCooldown.add(e.getPlayer());
                Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                    public void run() {
                        Methamphetamine.this.MCooldown.remove(e.getPlayer());
                    }
                }, 20L);
            }
        }

    }

    @EventHandler
    public void AcidAttack(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            Player p = (Player)e.getDamager();
            if (p.getInventory().getItemInHand().hasItemMeta()) {
                if (!Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.i.getHclmeta())) {
                    return;
                }

                p.getInventory().getItemInHand().setAmount(p.getInventory().getItemInHand().getAmount() - 1);
                p.getInventory().addItem(new ItemStack[]{new ItemStack(Material.BUCKET)});
                p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("ThrowAcid")), main));
                e.setDamage(10.0D);
                p.getWorld().playSound(p.getLocation(), Sound.BLOCK_LAVA_EXTINGUISH, 1.0F, 1.1F);
            }
        }

    }

    @EventHandler
    public void CancellingHCL(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (p.getInventory().getItemInHand().hasItemMeta() && Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.i.getHclmeta())) {
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void EphedraWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.WHEAT && FileManager.isValidLocation(block.getLocation())) {
            e.setCancelled(true);
            block.setType(Material.AIR);
            FileManager.removeLocation(block.getLocation());
            Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getEsitem(1));
        }

    }

    @EventHandler
    public void EphedraExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.WHEAT && FileManager.isValidLocation(block.getLocation())) {
                e.setCancelled(true);
                block.setType(Material.AIR);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getEsitem(1));
            }
        }

    }

    @EventHandler
    public void EphedraRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.WHEAT && FileManager.isValidLocation(block.getLocation())) {
                e.setCancelled(true);
                block.setType(Material.AIR);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getEsitem(1));
            }
        }

    }

    @EventHandler
    public void EphedraEntityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.WHEAT && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getEsitem(1));
            }
        }

    }
}
