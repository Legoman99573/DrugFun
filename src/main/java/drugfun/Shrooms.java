package drugfun;

import java.util.ArrayList;
import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

public class Shrooms implements Listener {
    Items i = new Items();
    ArrayList<Player> SCooldown = new ArrayList();
    private Main main;

    Shrooms(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onConsumeShrooms(final PlayerInteractEvent e) {
        if (e.getHand() == EquipmentSlot.HAND) {
            ItemStack ItemInHand = e.getPlayer().getItemInHand();
            final Player p = e.getPlayer();
            if (ItemInHand.hasItemMeta()) {
                if (ItemInHand == null || !Objects.equals(ItemInHand.getItemMeta(), this.i.getShroomMeta())) {
                    return;
                }

                if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (!e.getPlayer().hasPermission("drugfun.consumeshrooms")) {
                        e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Shrooms")), main));
                        e.setCancelled(true);
                        return;
                    }

                    if (!this.SCooldown.contains(e.getPlayer())) {
                        e.setCancelled(true);
                        ItemInHand.setAmount(ItemInHand.getAmount() - 1);
                        this.SCooldown.add(e.getPlayer());
                        p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(this.main.getConfig().getString("EatShrooms")), main));
                        p.addPotionEffects(EffectsManager.ShroomsEffects);
                        if (this.main.cs) {
                            p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_EAT, 1.0F, 1.0F);
                        }

                        if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                            ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setShrooms(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getShrooms() + 10);
                        }

                        SlurManager.addSlurFinish(e.getPlayer(), System.currentTimeMillis() + (long)(Main.shroomsSlurTime * 1000.0D));
                        SlurManager.setEffect(e.getPlayer(), Main.shroomsSlur);
                        Drugs.isOverdose(e.getPlayer(), Drugs.SHROOMS);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {
                            public void run() {
                                if (Shrooms.this.main.acs) {
                                    p.playSound(p.getLocation(), Sound.ENTITY_EVOKER_CELEBRATE, 0.6F, 1.0F);
                                }

                            }
                        }, 8L);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, new Runnable() {
                            public void run() {
                                Shrooms.this.SCooldown.remove(e.getPlayer());
                            }
                        }, 20L);
                    }
                }
            }
        }


    }

    public void onShroomsCraft() {
        NamespacedKey shroomskey = new NamespacedKey(this.main, "Shroomscraft");
        ShapelessRecipe shroomsCraft = new ShapelessRecipe(shroomskey, this.i.getShroomItem(1));
        shroomsCraft.addIngredient(Material.BROWN_MUSHROOM);
        shroomsCraft.addIngredient(Material.SUGAR);
        shroomsCraft.addIngredient(Material.BLAZE_POWDER);
        this.main.getServer().addRecipe(shroomsCraft);
    }
}
