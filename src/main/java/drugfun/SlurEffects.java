package drugfun;

public enum SlurEffects {
    NONE,
    ELLIPSES,
    NOVOWELS,
    EXCLAMATION,
    QUESTION,
    RANDOMIZE_WORDS;
}
