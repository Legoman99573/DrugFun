package drugfun;

import java.util.Iterator;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class SlurManager implements Listener {
    private static Main main;

    SlurManager(Main main) {
        SlurManager.main = main;
    }
    // $FF: synthetic field
    private static int[] $SWITCH_TABLE$drugfun$SlurEffects;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        if (Main.slurs.containsKey(p.getUniqueId()) && Main.slurseffect.containsKey(p.getUniqueId())) {
            SlurEffects effect = getEffect(p);
            String msg = e.getMessage();
            if (isOngoingEffect(p)) {
                Player player;
                Iterator var17;
                switch($SWITCH_TABLE$drugfun$SlurEffects()[effect.ordinal()]) {
                    case 2:
                        e.setCancelled(true);
                        String[] split = msg.split(" ");
                        StringBuilder sb = new StringBuilder();

                        for(int i = 0; i < split.length; ++i) {
                            sb.append(split[i] + "...");
                        }

                        var17 = Bukkit.getOnlinePlayers().iterator();

                        while(var17.hasNext()) {
                            player = (Player)var17.next();
                            player.sendMessage(convertToFormat(player, sb.toString()));
                        }

                        return;
                    case 3:
                        e.setCancelled(true);
                        msg = msg.replace("a", "");
                        msg = msg.replace("A", "");
                        msg = msg.replace("e", "");
                        msg = msg.replace("E", "");
                        msg = msg.replace("o", "");
                        msg = msg.replace("O", "");
                        msg = msg.replace("u", "");
                        msg = msg.replace("U", "");
                        msg = msg.replace("Y", "");
                        msg = msg.replace("Y", "");
                        var17 = Bukkit.getOnlinePlayers().iterator();

                        while(var17.hasNext()) {
                            player = (Player)var17.next();
                            player.sendMessage(convertToFormat(player, msg));
                        }

                        return;
                    case 4:
                        e.setCancelled(true);
                        String[] split2 = msg.split(" ");
                        StringBuilder sb2 = new StringBuilder();

                        for(int i = 0; i < split2.length; ++i) {
                            sb2.append(split2[i].toUpperCase() + "! ");
                        }

                        Iterator var20 = Bukkit.getOnlinePlayers().iterator();

                        while(var20.hasNext()) {
                            player = (Player)var20.next();
                            player.sendMessage(convertToFormat(player, sb2.toString()));
                        }

                        return;
                    case 5:
                        e.setCancelled(true);
                        String[] splitted = msg.split(" ");
                        StringBuilder sbt = new StringBuilder();

                        for(int i = 0; i < splitted.length; ++i) {
                            String[] indivsplit = splitted[i].split("");
                            StringBuilder tempsbt = new StringBuilder();

                            for(int j = 1; j <= indivsplit.length; ++j) {
                                if (j % 2 != 0) {
                                    tempsbt.append(indivsplit[j - 1].toLowerCase());
                                } else {
                                    tempsbt.append(indivsplit[j - 1].toUpperCase());
                                }
                            }

                            sbt.append(tempsbt.toString() + "? ");
                        }

                        Iterator var22 = Bukkit.getOnlinePlayers().iterator();

                        while(var22.hasNext()) {
                            player = (Player)var22.next();
                            player.sendMessage(convertToFormat(player, sbt.toString()));
                        }
                }
            }
        }

    }

    static void addSlurFinish(Player p, Long finishTime) {
        if (!Main.slurs.containsKey(p.getUniqueId())) {
            Main.slurs.put(p.getUniqueId(), finishTime);
        } else {
            Main.slurs.replace(p.getUniqueId(), finishTime);
        }
    }

    static Boolean isOngoingEffect(Player p) {
        return System.currentTimeMillis() <= (Long)Main.slurs.get(p.getUniqueId()) ? true : false;
    }

    static SlurEffects getEffect(Player p) {
        return (SlurEffects)Main.slurseffect.get(p.getUniqueId());
    }

    static void setEffect(Player p, SlurEffects se) {
        if (!Main.slurseffect.containsKey(p.getUniqueId())) {
            Main.slurseffect.put(p.getUniqueId(), se);
        } else {
            Main.slurseffect.replace(p.getUniqueId(), se);
        }
    }

    static String convertToFormat(Player p, String msg) {
        String str = p.getDisplayName();
        String rawmsg = Main.slurtextformat;
        rawmsg = rawmsg.replace("{PLAYER}", str);
        rawmsg = rawmsg.replace("{MESSAGE}", msg);
        return HexChat.translateHexCodes(rawmsg, main);
    }

    // $FF: synthetic method
    static int[] $SWITCH_TABLE$drugfun$SlurEffects() {
        int[] var10000 = $SWITCH_TABLE$drugfun$SlurEffects;
        if (var10000 != null) {
            return var10000;
        } else {
            int[] var0 = new int[SlurEffects.values().length];

            try {
                var0[SlurEffects.ELLIPSES.ordinal()] = 2;
            } catch (NoSuchFieldError var6) {
            }

            try {
                var0[SlurEffects.EXCLAMATION.ordinal()] = 4;
            } catch (NoSuchFieldError var5) {
            }

            try {
                var0[SlurEffects.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError var4) {
            }

            try {
                var0[SlurEffects.NOVOWELS.ordinal()] = 3;
            } catch (NoSuchFieldError var3) {
            }

            try {
                var0[SlurEffects.QUESTION.ordinal()] = 5;
            } catch (NoSuchFieldError var2) {
            }

            try {
                var0[SlurEffects.RANDOMIZE_WORDS.ordinal()] = 6;
            } catch (NoSuchFieldError var1) {
            }

            $SWITCH_TABLE$drugfun$SlurEffects = var0;
            return var0;
        }
    }
}