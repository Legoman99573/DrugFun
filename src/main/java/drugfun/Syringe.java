package drugfun;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class Syringe implements Listener {
    private static Main main;
    Items i = new Items();

    public Syringe(Main main) {
        Syringe.main = main;
    }

    @EventHandler
    public void InsertDrug(InventoryClickEvent e) {
        if (e.isLeftClick() && e.getCurrentItem() != null) {
            if (!e.getCurrentItem().hasItemMeta()) {
                return;
            }

            if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrMeta())) {
                if (!Objects.requireNonNull(e.getCursor()).hasItemMeta()) {
                    return;
                }

                if (Objects.equals(e.getCursor().getItemMeta(), this.i.getCitemmeta())) {
                    if (e.getCurrentItem().getAmount() > 1) {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setAmount(e.getCurrentItem().getAmount() - 1);
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getSyrCocaineItem(1)});
                    } else {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setItemMeta(this.i.getSyrCocaineMeta());
                    }
                }

                if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrMeta()) && Objects.equals(e.getCursor().getItemMeta(), this.i.getHitemmeta())) {
                    if (e.getCurrentItem().getAmount() > 1) {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setAmount(e.getCurrentItem().getAmount() - 1);
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getSyrHeroinItem(1)});
                    } else {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setItemMeta(this.i.getSyrHeroinMeta());
                    }
                }

                if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrMeta()) && Objects.equals(e.getCursor().getItemMeta(), this.i.getMethmeta())) {
                    if (e.getCurrentItem().getAmount() > 1) {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setAmount(e.getCurrentItem().getAmount() - 1);
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getSyrMethItem(1)});
                    } else {
                        e.setCancelled(true);
                        e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                        e.getCurrentItem().setItemMeta(this.i.getSyrMethMeta());
                    }
                }
            }
        }

    }

    @EventHandler
    public void TakeOutDrugs(InventoryClickEvent e) {
        if (e.isRightClick() && e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta()) {
            if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrCocaineMeta())) {
                e.setCancelled(true);
                e.setCurrentItem(this.i.getSyrItem(1));
                if (Objects.requireNonNull(e.getCursor()).hasItemMeta()) {
                    if (Objects.equals(e.getCursor().getItemMeta(), this.i.getCitemmeta())) {
                        if (e.getCursor().getAmount() == 64) {
                            e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getCitem(1)});
                        } else {
                            e.getCursor().setAmount(e.getCursor().getAmount() + 1);
                        }
                    } else {
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getCitem(1)});
                    }
                } else if (e.getCursor().getAmount() == 0) {
                    e.setCursor(this.i.getCitem(1));
                } else {
                    e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getCitem(1)});
                }
            }

            if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrHeroinMeta())) {
                e.setCancelled(true);
                e.setCurrentItem(this.i.getSyrItem(1));
                if (Objects.requireNonNull(e.getCursor()).hasItemMeta()) {
                    if (Objects.equals(e.getCursor().getItemMeta(), this.i.getHitemmeta())) {
                        if (e.getCursor().getAmount() == 64) {
                            e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getHitem(1)});
                        } else {
                            e.getCursor().setAmount(e.getCursor().getAmount() + 1);
                        }
                    } else {
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getHitem(1)});
                    }
                } else if (e.getCursor().getAmount() == 0) {
                    e.setCursor(this.i.getHitem(1));
                } else {
                    e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getHitem(1)});
                }
            }

            if (Objects.equals(e.getCurrentItem().getItemMeta(), this.i.getSyrMethMeta())) {
                e.setCancelled(true);
                e.setCurrentItem(this.i.getSyrItem(1));
                if (Objects.requireNonNull(e.getCursor()).hasItemMeta()) {
                    if (Objects.equals(e.getCursor().getItemMeta(), this.i.getMethmeta())) {
                        if (e.getCursor().getAmount() == 64) {
                            e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getMethitem(1)});
                        } else {
                            e.getCursor().setAmount(e.getCursor().getAmount() + 1);
                        }
                    } else {
                        e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getMethitem(1)});
                    }
                } else if (e.getCursor().getAmount() == 0) {
                    e.setCursor(this.i.getMethitem(1));
                } else {
                    e.getWhoClicked().getInventory().addItem(new ItemStack[]{this.i.getMethitem(1)});
                }
            }
        }

    }

    @EventHandler
    public void CancelDamage(PlayerInteractEvent e) {
        ItemMeta metaHand = e.getPlayer().getInventory().getItemInHand().getItemMeta();
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (!e.getPlayer().getInventory().getItemInHand().hasItemMeta()) {
                return;
            }

            assert metaHand != null;
            if (metaHand.equals(this.i.getSyrCocaineMeta()) || metaHand.equals(this.i.getSyrMeta()) || metaHand.equals(this.i.getSyrHeroinMeta()) || metaHand.equals(this.i.getSyrMethMeta())) {
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void NoDurability(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            Player p = (Player)e.getDamager();
            if (!p.getInventory().getItemInHand().hasItemMeta()) {
                return;
            }

            if (!Objects.equals(p.getInventory().getItemInHand().getItemMeta(), this.i.getSyrMeta())) {
                return;
            }

            e.setCancelled(true);
        }

    }

    public static void SyringeCraft() {
        Items i = new Items();
        NamespacedKey syringekey = new NamespacedKey(main, "CraftingSyringe");
        ShapedRecipe sc = new ShapedRecipe(syringekey, i.getSyrItem(1));
        sc.shape(new String[]{"000", " 1 ", " 1 "});
        sc.setIngredient('0', Material.STICK);
        sc.setIngredient('1', Material.GLASS);
        main.getServer().addRecipe(sc);
    }
}
