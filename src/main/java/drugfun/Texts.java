package drugfun;

import java.util.Objects;

public class Texts {
    static String prefix;
    static String cocaineSyringeInject;
    static String cocaineSyringeInjectVictim;
    static String cocaineSyringeInjectAttacker;
    static String cocaineSnort;
    static String cocaPlant;
    static String cocaHarvest;
    static String cocaHarvestFail;
    static String drugdealername;
    static String nopermission;
    static String drugdealerremoved;
    static String playernotonline;
    static String drugDealerSpawn;
    static String forcesave;
    static String reloadsuccess;
    static String reloadfailed;
    static String reloadfailedinvalid;
    static String help;
    static String helpmenu;
    static String helpshop;
    static String helpgive;
    static String helpdrugdealer;
    static String helpbloodtest;
    static String helpforcesave;

    Texts(Main main) {
        prefix = HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("Prefix")), main);
        cocaineSyringeInject = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("selfInjectCocaine")), main);
        cocaineSyringeInjectVictim = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("PlayerInjectPlayerCocaineVictim")), main);
        cocaineSyringeInjectAttacker = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("PlayerInjectPlayerCocaineAttacker")), main);
        cocaineSnort = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("SnortCocaine")), main);
        cocaPlant = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("CocaPlant")), main);
        cocaHarvest = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("CocaPlantHarvest")), main);
        cocaHarvestFail = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("CocaPlantHarvestFail")), main);
        drugdealername = HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("DrugDealerName")), main);
        nopermission = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("NoPermission")), main);
        drugdealerremoved = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("DrugDealerRemoved")), main);
        playernotonline = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("playerNotOnline")), main);
        drugDealerSpawn = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("drugDealerSpawn")), main);
        forcesave = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("forcesave")), main);
        reloadsuccess = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("reload.success")), main);
        reloadfailed = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("reload.failed")), main);
        reloadfailedinvalid = prefix + HexChat.translateHexCodes(Objects.requireNonNull(main.getConfig().getString("reload.invalid-config")), main);

        // Help Menu
        help = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.help")), main);
        helpmenu = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.menu")), main);
        helpshop = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.shop")), main);
        helpgive = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.give")), main);
        helpdrugdealer = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.drugdealerspawnremove")), main);
        helpbloodtest = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.bloodtest")), main);
        helpforcesave = HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("help.forcesave")), main);
    }
}
