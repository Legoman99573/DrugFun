package drugfun;

public enum VersionEnums {
    VERSION_18(0),
    VERSION_19(1),
    VERSION_110(2),
    VERSION_111(3),
    VERSION_112(4),
    VERSION_113(5),
    VERSION_114(6),
    VERSION_115(7),
    VERSION_116(8),
    OTHER_VERSION(-1);

    private int version;

    private VersionEnums(int versionInt) {
        this.version = versionInt;
    }

    public int getVersionInt() {
        return this.version;
    }
}