package drugfun;

import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Weed implements Listener {
    private static Main main;
    public ArrayList<Player> WCooldown = new ArrayList();
    Items i = new Items();
    HashMap<Location, Long> WeedTimer = new HashMap();
    Random r = new Random();
    Items items = new Items();
    ArrayList<Entity> ent;

    public Weed(Main main) {
        Weed.main = main;
    }

    @EventHandler
    public void cancelTorchPlace(BlockPlaceEvent e) {
        if (e.getBlock() != null && e.getPlayer().getInventory().getItemInHand().hasItemMeta() && e.getPlayer().getInventory().getItemInHand().getItemMeta().equals(this.i.getLitbluntmeta())) {
            e.setCancelled(true);
        }

    }


    @EventHandler
    public void LightTheBlunt(InventoryClickEvent e) {
        if (e.getClick().isRightClick() && e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getType().equals(Material.STICK) && e.getCurrentItem().getItemMeta().equals(this.i.getBluntitemmeta()) && e.getCursor().getType().equals(Material.FLINT_AND_STEEL)) {
            ItemStack FlintAndSteel = new ItemStack(Material.FLINT_AND_STEEL);
            ItemMeta SwordMeta = FlintAndSteel.getItemMeta();
            assert SwordMeta != null;

            if (e.getCurrentItem().getAmount() == 1) {
                e.getCurrentItem().setItemMeta(this.i.getLitbluntmeta());
                SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, Material.FLINT_AND_STEEL.getMaxDurability() / 5, true);
                FlintAndSteel.setDurability((short) 0);
                FlintAndSteel.setItemMeta(SwordMeta);
                // player.getInventory().addItem(itemStack);
                // e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getAmount() > 1) {
                e.getCurrentItem().setItemMeta(this.i.getLitbluntmeta());
                SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, Material.FLINT_AND_STEEL.getMaxDurability() / 5, true);
                FlintAndSteel.setDurability((short) 0);
                FlintAndSteel.setItemMeta(SwordMeta);
                // player.getInventory().addItem(itemStack);
                // e.getCursor().setAmount(e.getCursor().getAmount() - 1);
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void HitBlunt(final PlayerInteractEvent e) {
        if ((e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && e.getPlayer().getItemInHand().hasItemMeta() && e.getPlayer().getItemInHand().getItemMeta().equals(this.i.getLitbluntmeta())) {
            if (!e.getHand().equals(EquipmentSlot.HAND)) {
                return;
            }

            if (!e.getPlayer().hasPermission("drugfun.consumeweed")) {
                e.getPlayer().sendMessage(HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("nopermstoconsume").replace("%drugs%", "Weed")), main));
                e.setCancelled(true);
                return;
            }

            Player p = e.getPlayer();
            if (this.WCooldown.contains(p)) {
                return;
            }

            p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
            p.addPotionEffects(EffectsManager.WeedEffects);
            p.sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HitBlunt")), main));
            if (main.cs) {
                p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_BURN, 1.0F, 0.7F);
            }

            if (Main.bloods.containsKey(e.getPlayer().getUniqueId())) {
                ((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).setWeed(((DrugBloodManager)Main.bloods.get(e.getPlayer().getUniqueId())).getWeed() + 10);
            }

            Drugs.isOverdose(e.getPlayer(), Drugs.WEED);
            e.setCancelled(true);
            this.WCooldown.add(p);
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    Weed.this.WCooldown.remove(e.getPlayer());
                }
            }, 20L);
        }

    }

    @EventHandler
    public void onWeedPlant(BlockPlaceEvent event) {
        Player p = event.getPlayer();
        Block block = event.getBlock();
        if (p.getItemInHand() != null && p.getItemInHand().hasItemMeta() && Objects.equals(p.getItemInHand().getItemMeta(), this.i.getWsitemmeta())) {
            if (block.getBlockData() instanceof Bisected && ((Bisected)block.getBlockData()).getHalf() == Half.TOP) {
                block = block.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock();
            }
            event.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("WeedPlant")), main));
            FileManager.putLocation(block.getLocation(), (long)((double)System.currentTimeMillis() + main.WeedTime * 60.0D * 1000.0D));
            FileManager.putLocation(block.getLocation().add(0.0D, 1.0D, 0.0D), (long)((double)System.currentTimeMillis() + main.WeedTime * 60.0D * 1000.0D));
            if (main.dps) {
                p.playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);
            }
        }
    }

    @EventHandler
    public void WeedHarvest(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (Objects.equals(e.getHand(), EquipmentSlot.HAND)) {
                if (Objects.requireNonNull(e.getClickedBlock()).getType().equals(Material.LARGE_FERN) && FileManager.isValidLocation(e.getClickedBlock().getLocation())) {
                    if (System.currentTimeMillis() >= FileManager.getLocationTime(e.getClickedBlock().getLocation())) {
                        Objects.requireNonNull(e.getClickedBlock().getLocation().getWorld()).dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getWitem(main.WeedAmount));
                        if (((ItemStack)Main.weedList.get(this.r.nextInt(Main.weedList.size() - 1))).getType() == Material.DIAMOND) {
                            e.getClickedBlock().getLocation().getWorld().dropItemNaturally(e.getClickedBlock().getLocation(), this.items.getWsitem(main.weedplantAmount));
                        }

                        FileManager.setTime(e.getClickedBlock().getLocation(), (long)((double)System.currentTimeMillis() + main.WeedTime * 60.0D * 1000.0D));
                        e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HarvestMari")), main));
                        if (main.dhs) {
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.7F);
                        }
                    } else {
                        e.getPlayer().sendMessage(Texts.prefix + HexChat.translateHexCodes( Objects.requireNonNull(main.getConfig().getString("HarvestMariFail")), main));
                    }
                }

            }
        }
    }

    @EventHandler
    public void Break(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.LARGE_FERN && FileManager.isValidLocation(e.getBlock().getLocation())) {
            FileManager.removeLocation(e.getBlock().getLocation());
            e.setDropItems(false);
        }

    }

    @EventHandler
    public void CancellingFernDrop(ItemSpawnEvent e) {
        if (e.getEntity().getItemStack().getType() == Material.FERN) {
            e.setCancelled(true);
        }
    }
    @EventHandler
    public void WeedWaterBreak(BlockFromToEvent e) {
        Block block = e.getToBlock();
        if (block.getType() == Material.LARGE_FERN && FileManager.isValidLocation(block.getLocation())) {
            if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                e.setCancelled(true);
                block.setType(Material.AIR);
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
            }

            if (FileManager.isValidLocation(block.getLocation())) {
                e.setCancelled(true);
                block.setType(Material.AIR);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
            }
        }

    }

    @EventHandler
    public void WeedExtendEvent(BlockPistonExtendEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.LARGE_FERN) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
                }
            }
        }

    }

    @EventHandler
    public void WeedRetractEvent(BlockPistonRetractEvent e) {
        Iterator var2 = e.getBlocks().iterator();

        while(var2.hasNext()) {
            Block block = (Block)var2.next();
            if (block.getType() == Material.LARGE_FERN) {
                if (block.getRelative(BlockFace.UP).getBlockData() instanceof Bisected && FileManager.isValidLocation(block.getRelative(BlockFace.UP).getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getRelative(BlockFace.UP).getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
                }

                if (FileManager.isValidLocation(block.getLocation())) {
                    e.setCancelled(true);
                    block.setType(Material.AIR);
                    FileManager.removeLocation(block.getLocation());
                    Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
                }
            }
        }

    }

    @EventHandler
    public void WeedEntityExplode(EntityExplodeEvent e) {
        List<Block> explodedBlocks = e.blockList();
        Iterator itr = explodedBlocks.iterator();

        while(itr.hasNext()) {
            Block block = (Block)itr.next();
            if (block.getType() == Material.LARGE_FERN && FileManager.isValidLocation(block.getLocation())) {
                e.setYield((float) 0.0);
                FileManager.removeLocation(block.getLocation());
                Objects.requireNonNull(block.getLocation().getWorld()).dropItemNaturally(block.getLocation(), this.i.getWsitem(1));
            }
        }

    }
}
